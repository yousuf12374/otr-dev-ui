#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  9 00:43:21 2021

@author: yousuf
"""

from scipy.spatial.distance import cdist

import numpy as np
from tqdm import tqdm
import pandas as pd
import os

df=pd.read_csv("/home/yousuf/Downloads/scalecapacity/text_proj/gold_data/gold_wanted_customer_05032021_.csv",
               parse_dates=['booked_on','picked_up_by'])
df=df[df['created_year']==2020]
# df1=df[df['customer']=='DOW CHEMICAL COMPANY C/O XPO LOGISTICS']
df['travel_data']=df[['origin_data', 'destination_data']].agg('->'.join, axis=1)
df['year_week_str']=df[['created_year', 'created_week']].agg(' Week '.join, axis=1)

df1=df[df['travel_data']=='KY LOUISVILLE->PA ALIQUIPPA']

lanes=df.groupby(['travel_data']).count().sort_values(by=['id'],
					ascending=False)['id'][:5000].index.tolist()
year_weeks=df['year_week'].unique().tolist()
year_weeks.sort()

all_prices_lanes=[]
all_loads_lanes=[]
all_lanes=[]
all_shippers_lanes=[]
all_receivers_lanes=[]

for lane in tqdm(lanes,total=len(lanes)):
    df2=df[df['travel_data']==lane]
    all_shippers_lanes.append(df2['shipper'].unique().tolist())
    all_receivers_lanes.append(df2['receiver'].unique().tolist())
    prices=[]
    loads=[]
    for year_week in year_weeks:
        p=df2[df2['year_week']==year_week]['customer_rate'].mean()
        if str(p)=='nan':
            p=0
        prices.append(int(p))
        p=df2[df2['year_week']==year_week]
        loads.append(int(len(p)))
    all_prices_lanes.append(prices)
    all_loads_lanes.append(loads)
    all_lanes.append([prices,loads])

import pickle

with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/lanes.txt", "wb") as fp:   #Pickling
  pickle.dump(lanes, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_lanes.txt", "wb") as fp:   #Pickling
  pickle.dump(all_lanes, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_shippers_lanes.txt", "wb") as fp:   #Pickling
  pickle.dump(all_shippers_lanes, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_receivers_lanes.txt", "wb") as fp:   #Pickling
  pickle.dump(all_receivers_lanes, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_prices_lanes.txt", "wb") as fp:   #Pickling
  pickle.dump(all_prices_lanes, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_loads_lanes.txt", "wb") as fp:   #Pickling
  pickle.dump(all_loads_lanes, fp)

with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/lanes.txt", "rb") as fp:   # Unpickling
  lanes = pickle.load(fp)  
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_lanes.txt", "rb") as fp:   # Unpickling
  all_lanes = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_shippers_lanes.txt", "rb") as fp:   # Unpickling
  all_shippers_lanes = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_receivers_lanes.txt", "rb") as fp:   # Unpickling
  all_receivers_lanes = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_prices_lanes.txt", "rb") as fp:   # Unpickling
  all_prices_lanes = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/lanes/all_loads_lanes.txt", "rb") as fp:   # Unpickling
  all_loads_lanes = pickle.load(fp)


lane='KY LOUISVILLE->PA ALIQUIPPA'
df2=df[df['travel_data']==lane]
current_prices_lanes=[]
current_loads_lanes=[]
current_shipper_lane=[]
current_receiver_lane=[]
current_shipper_lane.append(df2['shipper'].unique().tolist())
current_receiver_lane.append(df2['receiver'].unique().tolist())
for year_week in year_weeks:
    p=df2[df2['year_week']==year_week]['customer_rate'].mean()
    if str(p)=='nan':
        p=0
    current_prices_lanes.append(int(p))
    p=df2[df2['year_week']==year_week]
    current_loads_lanes.append(int(len(p)))
current_lane=[current_prices_lanes,current_loads_lanes]

similarity_lanes=list(map(lambda x : cdist(np.array(current_lane),np.array(x),'cosine')[0][1]\
             +cdist(np.array(current_lane),np.array(x),'cosine')[1][0], all_lanes))

from math import*

def jaccard_similarity(x,y):
 
    intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
    union_cardinality = len(set.union(*[set(x), set(y)]))
    return intersection_cardinality/float(union_cardinality)

similiarity_shippers_receivers_lanes=list(map(lambda x : jaccard_similarity(x,current_shipper_lane[0]), all_shippers_lanes))



from operator import add
similarity=list( map(add, similarity_lanes, similiarity_shippers_receivers_lanes) )
similarity_=[x for x in similarity if x!=2 and x!=3 and x!=1]
[i[0] for i in sorted(enumerate(similarity_), key=lambda x:x[1])]
similarity_index=[i[0] for i in sorted(enumerate(similarity_), key=lambda x:x[1])]
similarity_index.reverse()










customers=df.groupby(['customer']).count().sort_values(by=['id'],
					ascending=False)['id'][:500000].index.tolist()
year_weeks=df['year_week'].unique().tolist()
year_weeks.sort()

all_prices_customers=[]
all_loads_customers=[]
all_customers=[]
all_shippers_customers=[]
all_receivers_customers=[]
all_lanes_customers=[]

for customer in tqdm(customers,total=len(customers)):
    df2=df[df['customer']==customer]
    all_shippers_customers.append(df2['shipper'].unique().tolist())
    all_receivers_customers.append(df2['receiver'].unique().tolist())
    all_lanes_customers.append(df2['travel_data'].unique().tolist())
    prices=[]
    loads=[]
    for year_week in year_weeks:
        p=df2[df2['year_week']==year_week]['customer_rate'].mean()
        if str(p)=='nan':
            p=0
        prices.append(int(p))
        p=df2[df2['year_week']==year_week]
        loads.append(int(len(p)))
    all_prices_customers.append(prices)
    all_loads_customers.append(loads)
    all_customers.append([prices,loads])

import pickle

with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/customers.txt", "wb") as fp:   #Pickling
  pickle.dump(customers, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_customers.txt", "wb") as fp:   #Pickling
  pickle.dump(all_customers, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_shippers_customers.txt", "wb") as fp:   #Pickling
  pickle.dump(all_shippers_customers, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_receivers_customers.txt", "wb") as fp:   #Pickling
  pickle.dump(all_receivers_customers, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_lanes_customers.txt", "wb") as fp:   #Pickling
  pickle.dump(all_lanes_customers, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_prices_customers.txt", "wb") as fp:   #Pickling
  pickle.dump(all_prices_customers, fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_loads_customers.txt", "wb") as fp:   #Pickling
  pickle.dump(all_loads_customers, fp)
  
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/customers.txt", "rb") as fp:   # Unpickling
  customers = pickle.load(fp)  
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_customers.txt", "rb") as fp:   # Unpickling
  all_customers = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_shippers_customers.txt", "rb") as fp:   # Unpickling
  all_shippers_customers = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_receivers_customers.txt", "rb") as fp:   # Unpickling
  all_receivers_customers = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_lanes_customers.txt", "rb") as fp:   # Unpickling
  all_lanes_customers = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_prices_customers.txt", "rb") as fp:   # Unpickling
  all_prices_customers = pickle.load(fp)
with open("/home/yousuf/Downloads/scalecapacity/text_proj/customers/all_loads_customers.txt", "rb") as fp:   # Unpickling
  all_loads_customers = pickle.load(fp)

customer='ANCHOR GLASS-CORPORATE'
df2=df[df['customer']==customer]
current_prices_customer=[]
current_loads_customer=[]
current_shipper_customer=[]
current_receiver_customer=[]
current_lanes_customer=[]

current_shipper_customer.append(df2['shipper'].unique().tolist())
current_receiver_customer.append(df2['receiver'].unique().tolist())
current_lanes_customer.append(df2['travel_data'].unique().tolist())

for year_week in year_weeks:
    p=df2[df2['year_week']==year_week]['customer_rate'].mean()
    if str(p)=='nan':
        p=0
    current_prices_customer.append(int(p))
    p=df2[df2['year_week']==year_week]
    current_loads_customer.append(int(len(p)))
current_customer=[current_prices_customer,current_loads_customer]

similarity_customers=list(map(lambda x : cdist(np.array(current_customer),np.array(x),'cosine')[0][1]\
             +cdist(np.array(current_customer),np.array(x),'cosine')[1][0], all_customers))

similiarity_shippers_receivers_customers=list(map(lambda x : jaccard_similarity(x,current_shipper_customer[0]),
                                              all_shippers_customers))
similiarity_lanes_customers=list(map(lambda x : jaccard_similarity(x,current_lanes_customer[0]),
                                              all_lanes_customers))
loads_similarity = list(map(lambda x : cdist(np.array([current_loads_customer]),
                                             np.array([x]),'euclidean')[0][0], all_loads_customers))
max_loads_similarity=max(loads_similarity)
loads_similarity = [x/max_loads_similarity for x in loads_similarity]

price_similarity = list(map(lambda x : cdist(np.array([current_prices_customer]),
                                             np.array([x]),'euclidean')[0][0], all_prices_customers))
max_price_similarity=max(price_similarity)
price_similarity = [x/max_price_similarity for x in price_similarity]

# from operator import add
# similarity=list( map(add, similarity_customers, similiarity_shippers_receivers_customers) )

similarity= [((1-x[0]))\
             +((1-x[1]))\
             +(x[2])\
             +(x[3]) \
             for x in zip(loads_similarity,price_similarity,
                          similiarity_shippers_receivers_customers,
                           similiarity_lanes_customers )]
similarity_=[x for x in similarity]# if x!=2 and x!=3 and x!=1 and x!=4
similarity_index=[i[0] for i in sorted(enumerate(similarity_), key=lambda x:x[1])]
similarity_index.reverse()

custs = [customers[i] for i in similarity_index][0:20]
custs=[x for x in custs if not x == customer]
dfx=df[df['customer'].isin(custs)]

dfx_map=dfx.groupby(['customer']).agg({"id":"count",
                                       "customer_rate":"mean",
                                       "buy_cost":"mean"})









