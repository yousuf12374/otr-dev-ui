#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 24 17:09:17 2021

@author: yousuf
"""

import geopandas as gp
from shapely.geometry import LineString, Point, LinearRing
from math import sin, cos, sqrt, atan2, radians
import time
import shapely
def diff_2_points(lat1,lon1,lat2,lon2):
    # approximate radius of earth in km
    R = 6373.0
    
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)
    
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    
    distance = R * c
    return distance*1000
def dist_point_line(lst,pnt):
    lst=list(lst.coords)
    d=list(map(lambda x: Point(x[1],x[0]),lst ))
    dists=int(min(list(map(lambda x: diff_2_points(pnt.x,pnt.y,x.x,x.y),d ))))
    return dists
    
start=time.time()
# dirz="/home/yousuf/Downloads/scalecapacity/text_proj/usa+map/tl_2020_us_aiannh/tl_2020_us_aiannh.shp"
# dirz="/home/yousuf/Downloads/scalecapacity/text_proj/usa+map/cb_2018_us_cd116_500k/cb_2018_us_cd116_500k.shp"
# dirz="/home/yousuf/Downloads/scalecapacity/text_proj/usa+map/tl_2018_us_necta/tl_2018_us_necta.shp"
dirz="/home/yousuf/Downloads/scalecapacity/text_proj/usa+map/500Cities_City_11082016/CityBoundaries.shp"
dirz="/home/yousuf/Downloads/scalecapacity/tl_2020_us_zcta510/tl_2020_us_zcta510.shp"
# shp = gp.GeoDataFrame.from_file(dirz)
# shp=shp.head()
#LINESTRING (11.3938816 43.2958734, 11.3938897 43.2955782)
#shp['distances'] = shp.apply(lambda l: dist_point_line(l['geometry'],Point(44.410527, 8.893807)),axis=1)
print(f"took: [{int(time.time()-start)}] seconds")

dirz="/home/yousuf/Downloads/scalecapacity/text_proj//cb_2018_us_state_500k/cb_2018_us_state_500k.shp"
df_st= gp.GeoDataFrame.from_file(dirz)
df_st.to_crs(epsg=4326, inplace=True)
pops=df_st['ALAND'].values.tolist()
features=[]
locations_data=[]
for i in df_st.itertuples():
	if type(i[-1])==shapely.geometry.polygon.Polygon:
		coords = list(i[-1].exterior.coords)
		coordz=[[x[0],x[1]] for x in coords]
		# poly_=str(i[8]).replace("POLYGON ((","").replace("))","").replace(")","").replace("(","")
		# poly=poly_.split(",")
		# poly=[x.strip() for x in poly]
		# poly=[[float(x.split(" ")[0]),float(x.split(" ")[1])] for x in poly]

		loc_data=str(i[5])+" "+str(i[6])
		pop=(i[8]/max(pops))
		features.append({
			"type": "Feature",
			"properties": {"id":i[0],"pop":pop,"name":loc_data},
			"geometry": {
				"type": "Polygon",
				"coordinates": [coordz]
			}
		})
	else:
		locations_data.append(str(i[3])+" "+str(i[1]))
		for j in i[-1]:
			coords = list(j.exterior.coords)
			coordz=[[x[0],x[1]] for x in coords]
			# poly_=str(i[8]).replace("POLYGON ((","").replace("))","").replace(")","").replace("(","")
			# poly=poly_.split(",")
			# poly=[x.strip() for x in poly]
			# poly=[[float(x.split(" ")[0]),float(x.split(" ")[1])] for x in poly]

			loc_data=str(i[5])+" "+str(i[6])
			pop=(i[8]/max(pops))
			features.append({
				"type": "Feature",
				"properties": {"id":i[0],"pop":pop,"name":loc_data},
				"geometry": {
					"type": "Polygon",
					"coordinates": [coordz]
				}
			})






























