#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  1 14:56:06 2021

@author: yousuf
"""

import pandas as pd
import os
from tqdm import tqdm
loads_dtype={
         'created_week': int,
          'duration': float,
          'asset_shipment_origin_namedCoordinates_latitude': float,
          'asset_shipment_origin_namedCoordinates_longitude': float,
          'asset_shipment_destination_namedCoordinates_latitude': float,
          'asset_shipment_destination_namedCoordinates_longitude': float,
          'asset_shipment_origin_namedCoordinates_stateProvince': str,
          'asset_shipment_origin_namedCoordinates_city': str,
          'asset_shipment_destination_namedCoordinates_stateProvince': str,
          'asset_shipment_destination_namedCoordinates_city': str,
          'asset_assetId': str,
          'callback_userId': str,
          'callback_companyName': str,
         'destination_data': str,
          'travel_data': str,
         'origin_data': str,
          'created_at': str,
          'origin_zone': str,
          'destination_zone': str,
          'created_month': str
     }
trucks_dtype={
       'created_week': int,
      'duration': float,
      'asset_equipment_origin_namedCoordinates_latitude': float,
      'asset_equipment_origin_namedCoordinates_longitude': float,
      'asset_equipment_destination_place_namedCoordinates_latitude': float,
      'asset_equipment_destination_place_namedCoordinates_longitude': float,
      'asset_equipment_origin_namedCoordinates_stateProvince': str,
      'asset_equipment_origin_namedCoordinates_city': str,
      'asset_equipment_destination_place_namedCoordinates_stateProvince': str,
      'asset_equipment_destination_place_namedCoordinates_city': str,
      'asset_assetId': str,
      'callback_userId': str,
      'callback_companyName': str,
         'destination_data': str,
      'travel_data': str,
         'origin_data': str,
      'created_at': str,
    'origin_zone': str,
    'destination_zone': str,
    'created_month': str
     }
clss_loads=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
        'asset_shipment_equipmentType',
        'asset_shipment_origin_namedCoordinates_stateProvince',
        'asset_shipment_origin_namedCoordinates_city',
        'asset_shipment_origin_namedCoordinates_latitude',
        'asset_shipment_origin_namedCoordinates_longitude',
        'asset_shipment_destination_namedCoordinates_stateProvince',
        'asset_shipment_destination_namedCoordinates_city',
        'asset_shipment_destination_namedCoordinates_latitude',
        'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
        'destination_data', 'travel_data', 'duration', 'created_at',
        'created_week','origin_zone','destination_zone','created_month']
clss_trucks=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
           'asset_equipment_equipmentType',
           'asset_equipment_origin_namedCoordinates_stateProvince',
           'asset_equipment_origin_namedCoordinates_city',
           'asset_equipment_origin_namedCoordinates_latitude',
           'asset_equipment_origin_namedCoordinates_longitude',
           'asset_equipment_destination_place_namedCoordinates_stateProvince',
           'asset_equipment_destination_place_namedCoordinates_city',
           'asset_equipment_destination_place_namedCoordinates_latitude',
           'asset_equipment_destination_place_namedCoordinates_longitude',
            'origin_data',
            'destination_data', 'travel_data', 'duration', 'created_at',
            'created_week','origin_zone','destination_zone','created_month']
dirz="/home/yousuf/Downloads/scalecapacity/text_proj/loads_new/"
zone_df=pd.read_csv("/home/yousuf/Downloads/scalecapacity/us-zip-code-latitude-and-longitude.csv",delimiter=";",dtype={'Zip':str})

zone_df['location'] = zone_df[['State','City']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df['location'] = zone_df['location'].str.upper()

zone_df1=pd.read_csv("/home/yousuf/Downloads/scalecapacity/simplemaps_uszips_basicv1.77/uszips.csv",dtype={'county_fips':str})

zone_df1['location'] = zone_df1[['state_id','city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df1['location'] = zone_df1['location'].str.upper()

zone_df2=pd.read_csv("/home/yousuf/Downloads/scalecapacity/zipcodes-csv-10-Aug-2004/zipcode.csv",dtype={'zip':str})

zone_df2['location'] = zone_df2[['state','city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df2['location'] = zone_df2['location'].str.upper()

dct={}
# for i in zone_df.itertuples():
#     dct[i[9]]=i[1][0]
# for i in zone_df1.itertuples():
#     dct[i[19]]=i[11][0]
for i in zone_df2.itertuples():
    dct[i[8]]=i[1][0]
    
from sqlite3 import Error
import sqlite3 as lite
import os
dbname=f'{os.getcwd()}/db.sqlite3'
conn = lite.connect(dbname)
cursor = conn.cursor()    
cursor.execute("""CREATE TABLE IF NOT EXISTS loads (
              id INTEGER PRIMARY KEY,
              asset_assetId TEXT NOT NULL UNIQUE,
              callback_userId TEXT NOT NULL,
              callback_companyName TEXT NOT NULL,
              asset_status_created_date TEXT NOT NULL,
              asset_shipment_equipmentType TEXT NOT NULL,
              asset_shipment_origin_namedCoordinates_stateProvince TEXT NOT NULL,
              asset_shipment_origin_namedCoordinates_city TEXT NOT NULL,
              asset_shipment_origin_namedCoordinates_latitude TEXT NOT NULL,
              asset_shipment_origin_namedCoordinates_longitude TEXT NOT NULL,
              asset_shipment_destination_namedCoordinates_stateProvince TEXT NOT NULL,
              asset_shipment_destination_namedCoordinates_city TEXT NOT NULL,
              asset_shipment_destination_namedCoordinates_latitude TEXT NOT NULL,
              asset_shipment_destination_namedCoordinates_longitude TEXT NOT NULL,
              origin_data TEXT NOT NULL,
              destination_data TEXT NOT NULL,
              duration TEXT NOT NULL,
              created_at TEXT NOT NULL,
              created_week TEXT NOT NULL,
              origin_zone TEXT NOT NULL,
              destination_zone TEXT NOT NULL,
              created_month TEXT NOT NULL
              );""")
conn.commit()

cursor.execute("""CREATE TABLE IF NOT EXISTS trucks (
              id INTEGER PRIMARY KEY,
              asset_assetId TEXT NOT NULL UNIQUE,
              callback_userId TEXT NOT NULL,
              callback_companyName TEXT NOT NULL,
              asset_status_created_date TEXT NOT NULL,
              asset_equipment_equipmentType TEXT NOT NULL,
              asset_equipment_origin_namedCoordinates_stateProvince TEXT NOT NULL,
              asset_equipment_origin_namedCoordinates_city TEXT NOT NULL,
              asset_equipment_origin_namedCoordinates_latitude TEXT NOT NULL,
              asset_equipment_origin_namedCoordinates_longitude TEXT NOT NULL,
              asset_equipment_destination_place_namedCoordinates_stateProvince TEXT NOT NULL,
              asset_equipment_destination_place_namedCoordinates_city TEXT NOT NULL,
              asset_equipment_destination_place_namedCoordinates_latitude TEXT NOT NULL,
              asset_equipment_destination_place_namedCoordinates_longitude TEXT NOT NULL,
              origin_data TEXT NOT NULL,
              destination_data TEXT NOT NULL,
              duration TEXT NOT NULL,
              created_at TEXT NOT NULL,
              created_week TEXT NOT NULL,
              origin_zone TEXT NOT NULL,
              destination_zone TEXT NOT NULL,
              created_month TEXT NOT NULL
              );""")
conn.commit()
loads=[x for x in os.listdir(dirz) if x.endswith("csv")]
for k in tqdm(loads,total=len(loads)):
    df = pd.read_csv(dirz+k,
    		parse_dates=['asset_status_created_date'],
    	                 dtype=loads_dtype)
    # df['origin_zone'] = df[['origin_data','callback_userId']].apply(lambda row: dct.get(row['origin_data'],'x'), axis=1)
    # df['destination_zone'] = df[['destination_data','callback_userId']].apply(lambda row: dct.get(row['destination_data'],'x'), axis=1)
    df['created_month']=df['asset_status_created_date'].apply(lambda x : x.strftime("%B"))
    df.to_csv(dirz+k,index=0)
    df = pd.read_csv(dirz+k,
    		parse_dates=['asset_status_created_date'],
    	                 dtype=loads_dtype)
    df=df[clss_loads]
    for i in tqdm(df.itertuples(),total=len(df)):
        postgresql=f"""INSERT INTO loads (asset_assetId, callback_userId, callback_companyName, asset_status_created_date, asset_shipment_equipmentType,
        asset_shipment_origin_namedCoordinates_stateProvince, asset_shipment_origin_namedCoordinates_city,
        asset_shipment_origin_namedCoordinates_latitude, asset_shipment_origin_namedCoordinates_longitude,
        asset_shipment_destination_namedCoordinates_stateProvince, asset_shipment_destination_namedCoordinates_city,
        asset_shipment_destination_namedCoordinates_latitude, asset_shipment_destination_namedCoordinates_longitude,
        origin_data, destination_data,duration,created_at,created_week,origin_zone,destination_zone,created_month) values (
        "{i[1]}", "{i[2]}", "{i[3]}", "{str(i[4])}", 
        "{i[5]}", "{i[6]}", "{i[7]}", "{str(i[8])}", 
        "{str(i[9])}", "{str(i[10])}", "{str(i[11])}", "{str(i[12])}", 
        "{str(i[13])}", "{i[14]}", "{i[15]}",
        "{str(i[17])}", "{str(i[18])}", "{i[19]}", "{i[20]}", "{i[21]}", "{i[22]}") 
        ON CONFLICT (asset_assetId) DO UPDATE SET callback_userId="{i[2]}", asset_status_created_date="{str(i[4])}", 
        asset_shipment_origin_namedCoordinates_latitude="{str(i[8])}", asset_shipment_origin_namedCoordinates_longitude="{str(i[9])}", 
        asset_shipment_destination_namedCoordinates_latitude="{str(i[12])}", asset_shipment_destination_namedCoordinates_longitude="{str(i[13])}", 
        created_at="{str(i[18])}",origin_zone="{str(i[20])}",destination_zone="{str(i[21])}",created_month="{str(i[22])}"; """
        cursor.execute(postgresql)
    # conn.commit()

dirz="/home/yousuf/Downloads/scalecapacity/text_proj/trucks_new/"
trucks=[x for x in os.listdir(dirz) if x.endswith("csv")]

for k in tqdm(trucks,total=len(trucks)):
    df = pd.read_csv(dirz+k,
    		parse_dates=['asset_status_created_date'],
    	                 dtype=trucks_dtype)
    # df['origin_zone'] = df[['origin_data','callback_userId']].apply(lambda row: dct.get(row['origin_data'],'x'), axis=1)
    # df['destination_zone'] = df[['destination_data','callback_userId']].apply(lambda row: dct.get(row['destination_data'],'x'), axis=1)
    df['created_month']=df['asset_status_created_date'].apply(lambda x : x.strftime("%B"))
    df.to_csv(dirz+k,index=0)
    df = pd.read_csv(dirz+k,
    		parse_dates=['asset_status_created_date'],
    	                 dtype=trucks_dtype)
    df=df[clss_trucks]
    for i in tqdm(df.itertuples(),total=len(df)):
        postgresql=f"""INSERT INTO trucks (asset_assetId, callback_userId, callback_companyName, asset_status_created_date, asset_equipment_equipmentType,
        asset_equipment_origin_namedCoordinates_stateProvince, asset_equipment_origin_namedCoordinates_city,
        asset_equipment_origin_namedCoordinates_latitude, asset_equipment_origin_namedCoordinates_longitude,
        asset_equipment_destination_place_namedCoordinates_stateProvince, asset_equipment_destination_place_namedCoordinates_city,
        asset_equipment_destination_place_namedCoordinates_latitude, asset_equipment_destination_place_namedCoordinates_longitude,
        origin_data, destination_data,duration,created_at,created_week,origin_zone,destination_zone,created_month) values (
        "{i[1]}", "{i[2]}", "{i[3]}", "{str(i[4])}", 
        "{i[5]}", "{i[6]}", "{i[7]}", "{str(i[8])}", 
        "{str(i[9])}", "{str(i[10])}", "{str(i[11])}", "{str(i[12])}", 
        "{str(i[13])}", "{i[14]}", "{i[15]}",
        "{str(i[17])}", "{str(i[18])}", "{i[19]}", "{i[20]}", "{i[21]}", "{i[22]}") 
        ON CONFLICT (asset_assetId) DO UPDATE SET callback_userId="{i[2]}", asset_status_created_date="{str(i[4])}", 
        asset_equipment_origin_namedCoordinates_latitude="{str(i[8])}", asset_equipment_origin_namedCoordinates_longitude="{str(i[9])}", 
        asset_equipment_destination_place_namedCoordinates_latitude="{str(i[12])}", asset_equipment_destination_place_namedCoordinates_longitude="{str(i[13])}", 
        created_at="{str(i[18])}",origin_zone="{str(i[20])}",destination_zone="{str(i[21])}",created_month="{str(i[22])}"; """
        cursor.execute(postgresql)

    conn.commit()

conn.commit()
cursor.close()
conn.close()






















































