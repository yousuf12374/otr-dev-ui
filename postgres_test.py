#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  8 16:27:24 2021

@author: yousuf
"""

import psycopg2
from psycopg2 import Error
# Details:
# Host: otr-dev.ciwoso6g1uql.us-east-1.rds.amazonaws.com
# Port: 5432
# Username: otrdev
# pass: otr12dev34
# JumphostIP: 3.239.67.6
"""
try:
    # Connect to an existing database
    connection = psycopg2.connect(user="otr_just_maps",
                                  password="otrdev123321",
                                  host="otr.ciwoso6g1uql.us-east-1.rds.amazonaws.com",
                                  port="5432",
                                  database="postgres")

    # Create a cursor to perform database operations
    cursor = connection.cursor()
    # Print PostgreSQL details
    print("PostgreSQL server information")
    print(connection.get_dsn_parameters(), "\n")
    # Executing a SQL query
    cursor.execute("SELECT version();")
    # Fetch result
    record = cursor.fetchone()
    print("You are connected to - ", record, "\n")

except (Exception, Error) as error:
    print("Error while connecting to PostgreSQL", error)
finally:
    if (connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
"""    
# pgfutter --host "otr.ciwoso6g1uql.us-east-1.rds.amazonaws.com" --port "5432" --db "postgres" --table "loads_raw" --user "otr_just_maps" --pw "otrdev123321" csv /home/yousuf/Downloads/scalecapacity/text_proj/loads_new/loads_2021_03_3.csv

# pgfutter --db "postgres" --port "5432" --user "otr_just_maps" --pw "otrdev123321" csv /home/yousuf/Downloads/scalecapacity/text_proj/loads_new/loads_2021_03_3.csv

# psql --host=otr.ciwoso6g1uql.us-east-1.rds.amazonaws.com --port=5432 --username=otr_just_maps --password --dbname=postgres
sss="""
INSERT INTO sensitive_areas (area_id, name, area_size, type, zone) VALUES (
 1,
 'Summerhill Elementary School',
 67920.64,
 'school',
 ST_Polygon('polygon ((52 28, 58 28, 58 23, 52 23, 52 28))', 4326)
);

INSERT INTO hazardous_sites (row_id, site_id, name, location) VALUES (
 1,
 102,
 'W. H. Kleenare Chemical Repository',
 ST_Point('point (52 24)', 4326)
);

"""
# coords = "'((2,2),(3,4),(3,6),(1,1))'"  # note the "'"

# cursor.exeucte("INSERT INTO public.test(timestamp, poly) VALUES(NOW(), " + coords + ");")

connection = psycopg2.connect(user="otr_just_maps",
                              password="otrdev123321",
                              host="otr.ciwoso6g1uql.us-east-1.rds.amazonaws.com",
                              port="5432",
                              database="postgres")

# Create a cursor to perform database operations
cursor = connection.cursor()
# cursor.execute("CREATE EXTENSION postgis;")
# connection.commit()

cursor.execute("DROP TABLE map;")
connection.commit()
createTable = """  CREATE TABLE IF NOT EXISTS map
                (
                    id SERIAL PRIMARY KEY,
                    site_id varchar(255) NOT NULL UNIQUE,
                    f_name varchar(255) NOT NULL,
                    l_name varchar(255) NOT NULL,
                    geom GEOMETRY DEFAULT NULL
                );
                """
                  
cursor.execute(createTable)
connection.commit()

cursor.execute("INSERT INTO map (site_id, f_name,l_name, geom) VALUES (\
 111,\
 'Yousuf',\
 'M.',\
 ST_GeomFromText('POINT(-71.064544 42.28787)',4326)\
);")
connection.commit()
import pandas as pd
# df=pd.read_sql("select COUNT(*) from import.loads_raw ;",connection)
df1=pd.read_sql("select * from import.loads_raw LIMIT 10;",connection)
df2=pd.read_sql("select * from import.trucks_raw LIMIT 10;",connection)

cursor.close()
connection.close()
 # ST_Point('point (52 24)', 4326)\
# ALTER TABLE import.loads_raw ADD COLUMN origin_geom GEOGRAPHY(POINT,4326);

# UPDATE import.loads_raw SET origin_geom = 'SRID=4326;POINT(' || asset_shipment_origin_namedcoordinates_longitude || ' ' || asset_shipment_origin_namedcoordinates_latitude || ')';

# UPDATE import.loads_raw SET origin_geom = point(asset_shipment_origin_namedcoordinates_latitude, asset_shipment_origin_namedcoordinates_longitude);


#this si the one which is working
# ALTER TABLE import.loads_raw ADD COLUMN origin_geom GEOGRAPHY(POINT,4326);
# UPDATE import.loads_raw SET  origin_geom = st_Setsrid(st_Point("asset_shipment_origin_namedcoordinates_longitude"::double precision, "asset_shipment_origin_namedcoordinates_latitude"::double precision),4326);
# ALTER TABLE import.loads_raw ADD COLUMN destination_geom GEOGRAPHY(POINT,4326);
# UPDATE import.loads_raw SET  destination_geom = st_Setsrid(st_Point("asset_shipment_destination_namedcoordinates_longitude"::double precision, "asset_shipment_destination_namedcoordinates_latitude"::double precision),4326);
# ALTER TABLE import.trucks_raw ADD COLUMN origin_geom GEOGRAPHY(POINT,4326);
# UPDATE import.trucks_raw SET  origin_geom = st_Setsrid(st_Point("asset_equipment_origin_namedcoordinates_longitude"::double precision, "asset_equipment_origin_namedcoordinates_latitude"::double precision),4326);
# ALTER TABLE import.trucks_raw ADD COLUMN destination_geom GEOGRAPHY(POINT,4326);
# UPDATE import.trucks_raw SET  destination_geom = st_Setsrid(st_Point("asset_equipment_destination_place_namedcoordinates_longitude"::double precision, "asset_equipment_destination_place_namedcoordinates_latitude"::double precision),4326);












