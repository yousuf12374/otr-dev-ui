#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  2 11:02:29 2021

@author: yousuf
"""

from tqdm import tqdm
import pandas as pd
import os
colors={"Standard Van 53":"#FA3005",
        "Standard Reefer 53":"#1700FF",
        "Standard Van 48":"#FF0000",
        "Standard Reefer 48":"#008FFF"}
zone_df2=pd.read_csv("/home/yousuf/Downloads/scalecapacity/zipcodes-csv-10-Aug-2004/zipcode.csv",dtype={'zip':str})

zone_df2['location'] = zone_df2[['state','city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df2['location'] = zone_df2['location'].str.upper()

dct={}
for i in zone_df2.itertuples():
    dct[i[8]]=i[1][0]

# df=pd.read_csv("/home/yousuf/Downloads/scalecapacity/text_proj/gold_data/gold_wanted_customer.csv",
#                parse_dates=['booked_on'])
df=pd.read_csv("/home/yousuf/Downloads/scalecapacity/text_proj/gold_data/gold_wanted_customer_05032021_.csv",
               parse_dates=['booked_on','picked_up_by'])
# df['origin_data'] = df[['origin_state_province','origin_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['destination_data'] = df[['destination_state_province','destination_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['origin_data'] = df['origin_data'].str.upper()
# df['destination_data'] = df['destination_data'].str.upper()

# df['origin_zone'] = df[['origin_data','id']].apply(lambda row: dct.get(row['origin_data'],'x'), axis=1)

# df['destination_zone'] = df[['destination_data','id']].apply(lambda row: dct.get(row['destination_data'],'x'), axis=1)

# df=df[~df['booked_on'].astype(str).str.startswith('NaT')]
# # df['created_at'] = df['booked_on'].map(lambda x: x.strftime('%Y-%m-%d'))
# df['created_week']=df['booked_on'].apply(lambda x : x.strftime("%V"))
# df['created_week']=df['created_week'].astype(int)
# df['created_year']=df['booked_on'].apply(lambda x : x.strftime("%Y"))
# df['created_year']=df['created_year'].astype(int)
# df['created_month']=df['booked_on'].apply(lambda x : x.strftime("%B"))
# df['created_month_num']=df['booked_on'].apply(lambda x : x.strftime("%m"))
# df['created_month_num']=df['created_month_num'].astype(int)

# df=df[(df['origin_zone']!='x') & (df['destination_zone']!='x')]

# df=df[~((df['destination_lat'].astype(str).str.startswith('nan')) |\
#                 (df['origin_lat'].astype(str).str.startswith('nan')))]
# df['year_week']=df['created_year'].astype(str)+df['created_week'].astype(str)
# df['duration']=(df['picked_up_by'] - df['booked_on']) / pd.Timedelta(hours=1)
# df.to_csv("/home/yousuf/Downloads/scalecapacity/text_proj/gold_data/gold_wanted_customer_05032021_.csv",index=0)

df1=df[df['customer']=='DOW CHEMICAL COMPANY C/O XPO LOGISTICS']
# df1=df1[(df1['origin_zone']!='x') & (df1['destination_zone']!='x')]

df_map=df1.groupby(['shipper','receiver']).agg({"id":"count",
                                                "origin_lat":"max",
                                                "origin_lng":"max",
                                                "destination_lat":"max",
                                                "destination_lng":"max",
                                                "equipment_type":"max",
                                                "origin_zone":"max",
                                                "destination_zone":"max"}).reset_index()

shipper=[]
receiver=[]

df1['year_week']=df['created_year'].astype(str)+df['created_week'].astype(str)
shp_rcv=[]
locs={}
dats=list(set(df1['year_week'].unique().tolist()))
for i in dats:
    org=df1[df1['year_week']==i].reset_index()
    locs[str(i)]=[str(org['booked_on'].min()),str(org['booked_on'].max())]

df_map=df1.groupby(['origin_lat',
					'origin_lng',
                    'destination_lat',
					'destination_lng','year_week']).agg({"id":"count"}).reset_index()
for i in df_map.itertuples():
    ed=locs[str(i[5])][1].split("-")
    sd=locs[str(i[5])][0].split("-")
    tmp={
        "type":"Feature",
        "geometry":{
          "type":"LineString",
          "coordinates":[[i[1],i[2]],[i[3],i[4]]]
        },
        "properties":{
          "endDate":str(ed[1])+'/'+str(ed[2].split(" ")[0])+'/'+str(ed[0][-2:]),
          "startDate":str(sd[1])+'/'+str(sd[2].split(" ")[0])+'/'+str(sd[0][-2:])
        }
      }
    # shp_rcv.append([i[1],i[2],i[3],i[4],i[6],
    #              locs[str(i[5])]])
    shp_rcv.append(tmp)

df_map_r=df1.groupby(['origin_lat',
					'origin_lng','year_week']).agg({"id":"count"}).reset_index()
for i in df_map_r.itertuples():
	shipper.append([i[1],i[2],i[4],
                 locs[str(i[3])]])
df_map_s=df1.groupby(['destination_lat',
					'destination_lng','year_week'
                    ]).agg({"id":"count"}).reset_index()
for i in df_map_s.itertuples():
	receiver.append([i[1],i[2],i[4],
                 locs[str(i[3])]])

myData = {
  "type": "FeatureCollection",
  "features":shp_rcv
};




data=[]
max_counts=df_map['id'].max()

for i in df_map.itertuples():
    tmp=df1[df1['receiver']==i[2]]
    df_tmp=tmp.groupby(['receiver','equipment_type']).agg({"id":"count"}).reset_index()
    popup={'Receiver':i[2],
        "Standard Van 53":0,
        "Standard Reefer 53":0,
        "Standard Van 48":0,
        "Standard Reefer 48":0}
    for j in df_tmp.itertuples():
        popup[j[2]]=j[3]
    data.append([[i[4],i[5]],
                  [i[6],i[7]],
                  (int(i[3])/max_counts)+0.7,
                  colors[i[8]],
                  [popup]])



customer=['DOW CHEMICAL COMPANY C/O XPO LOGISTICS']
df1=df[(df['customer'].isin(customer))]
df_map=df1.groupby(['origin_data','destination_data','year_week']).agg({"id":"count",
                                               "equipment_type":"max",
                                               "booked_on":"first"}).reset_index()
locs={}
dats=list(set(df_map['origin_data'].unique().tolist()+df_map['destination_data'].unique().tolist()))
for i in dats:
	org=df1[df1['origin_data']==i].reset_index()
	if not len(org)>0:
		dest=df1[df1['destination_data']==i].reset_index()
		latlng=dest.values[0]
		locs[i]=[latlng[17],latlng[18]]
	else:
		latlng=org.values[0]
		locs[i]=[latlng[15],latlng[16]]

timez={}
dats=list(set(df1['year_week'].unique().tolist()))
for i in dats:
    org=df1[df1['year_week']==i].reset_index()
    timez[str(i)]=str(org['booked_on'].min())

data=[]
max_counts=df_map['id'].max()
if len(df_map)<30:
	multiplier=3.7
else:
	multiplier=8

df_map.sort_values(by=['booked_on'],ascending=False,inplace=True)
for i in df_map.itertuples():
	tmp=df1[(df1['destination_data']==i[2]) & (df1['origin_data']==i[1])]
	df_tmp=tmp.groupby(['origin_data',
						'destination_data',
					'equipment_type']).agg({"id":"count"}).reset_index()
	popup={'origin_data':i[1],
		'destination_data':i[2],
		"Standard Van 53":0,
		"Standard Reefer 53":0,
		"Standard Van 48":0,
		"Standard Reefer 48":0}
	for j in df_tmp.itertuples():
		popup[j[3]]=j[4]
	x=locs[i[1]]
	y=locs[i[2]]
	width=(int(i[4])/max_counts)*10
	if width<1:
		width=0.7
        
    t={
        "type": "LineString",
        "properties": {
              "popupContent": popup,
              "width":width,
              "color": colors[i[5]]
            },
        "coordinates": [x,y]
       }
	data.append([x,
				y,
				width,
				colors[i[5]],
				[popup]
                ])


customer=['SOPUS PRODUCTS','WALGREENS (INBOUND)','NATIONAL CONTAINER GROUP, LLC']
df1=df[(df['customer'].isin(customer))]

df1['travel_data']=df1[['origin_data', 'destination_data']].agg('->'.join, axis=1)
df1['year_month']=df1[['created_year', 'created_month']].agg(' '.join, axis=1)

df['travel_data']=df[['origin_data', 'destination_data']].agg('->'.join, axis=1)

carr=df1.groupby(['travel_data','year_week']).agg({'id':'count'}).reset_index()

lanes=carr.groupby(['travel_data']).count().sort_values(by=['id'],ascending=False)['id'][:20].index.tolist()
carr=carr[carr['travel_data'].isin(lanes)]
year_weeks=carr['year_week'].unique().tolist()
year_weeks.sort()
heats=[]
for i in lanes:
    vals=[]
    for j in year_weeks:
        try:
            val=carr[(carr['year_week']==j) & (carr['travel_data']==i)]['id'].values.tolist()[0]
        except IndexError as e:
            val=0
        vals.append(val)
    heats.append({
          "name": i,
          "data": vals
        }) 

ranges=[]
lst = range(1,carr['id'].max()+1)
nameheat=['low','medium','high']
colorheat=['#00A100','#128FD9','#FF0000']
for i in zip(np.array_split(lst, 3),nameheat,colorheat):
    ranges.append(
            {
    "from": int(i[0].min()),
    "to": int(i[0].max()),
    "name": i[1],
    "color": i[2]
  })



df_map=df1.groupby(['origin_zone','destination_zone']).agg({"id":"count",
                                                "origin_lat":"max",
                                                "origin_lng":"max",
                                                "destination_lat":"max",
                                                "destination_lng":"max",
                                                "equipment_type":"max"}).reset_index()

df_map=df_map[~((df_map['destination_lat'].astype(str).str.startswith('nan')) |\
                (df_map['origin_lat'].astype(str).str.startswith('nan')))]

data=[]
max_counts=df_map['id'].max()

for i in df_map.itertuples():
    tmp=df1[(df1['destination_zone']==i[2]) & (df1['origin_zone']==i[1])]
    df_tmp=tmp.groupby(['origin_zone',
                        'destination_zone',
                        'equipment_type']).agg({"id":"count"}).reset_index()
    popup={'origin_zone':i[1],
        'destination_zone':i[2],
        "Standard Van 53":0,
        "Standard Reefer 53":0,
        "Standard Van 48":0,
        "Standard Reefer 48":0}
    for j in df_tmp.itertuples():
        popup[j[3]]=j[4]
    data.append([[i[4],i[5]],
                  [i[6],i[7]],
                  (int(i[3])/max_counts)*10,
                  colors[i[8]],
                  [popup]])


df1=df[df['customer']=='DOW CHEMICAL COMPANY C/O XPO LOGISTICS']
df_map=df1.groupby(['origin_data','destination_data']).agg({"id":"count",
                                                "equipment_type":"min"}).reset_index()
import random
locs={}
dats=list(set(df_map['origin_data'].unique().tolist()+df_map['destination_data'].unique().tolist()))
for i in dats:
    org=df1[df1['origin_data']==i].reset_index()
    if not len(org)>0:
        dest=df1[df1['destination_data']==i].reset_index()
        latlng=dest.values[random.randint(0,len(dest)-1)]
        locs[i]=[latlng[17],latlng[18]]
    else:
        latlng=org.values[random.randint(0,len(org)-1)]
        locs[i]=[latlng[15],latlng[16]]



data=[]
max_counts=df_map['id'].max()

for i in df_map.itertuples():
    tmp=df1[(df1['destination_data']==i[2]) & (df1['origin_data']==i[1])]
    df_tmp=tmp.groupby(['origin_data',
                        'destination_data',
                        'equipment_type']).agg({"id":"count"}).reset_index()
    popup={'origin_data':i[1],
        'destination_data':i[2],
        "Standard Van 53":0,
        "Standard Reefer 53":0,
        "Standard Van 48":0,
        "Standard Reefer 48":0}
    for j in df_tmp.itertuples():
        popup[j[3]]=j[4]
    x=locs[i[1]]
    y=locs[i[2]]
    data.append([x,
                  y,
                  (int(i[3])/max_counts)*10,
                  colors[i[4]],
                  [popup]])




all_locs=df['origin_data'].values.tolist()+df['destination_data'].values.tolist()
all_locs=sorted(list(set(all_locs)))

circles_=[]
all_radiuses=[]
for i in tqdm(all_locs,total=len(all_locs)):
    tmp1=df[(df['origin_data']==i)].reset_index()
    tmp2=df[(df['destination_data']==i)].reset_index()
    if len(tmp1)>1:
        circles_.append([[tmp1['origin_lat'][0],tmp1['origin_lng'][0]],len(tmp1)+len(tmp2),i])
        all_radiuses.append(len(tmp1)+len(tmp2))
    elif len(tmp2)>1:
        circles_.append([[tmp2['destination_lat'][0],tmp2['destination_lng'][0]],len(tmp1)+len(tmp2),i])
        all_radiuses.append(len(tmp1)+len(tmp2))
    else:
        continue

circles=[]
for i in circles_:
    circles.append([i[0],i[1]/max(all_radiuses),i[2]])
import pickle
with open('/home/yousuf/Downloads/scalecapacity/text_proj/gold_data/circles_gd.txt', 'wb') as f:
   pickle.dump(circles, f)

with open('/home/yousuf/Downloads/scalecapacity/text_proj/gold_data/circles_gd.txt', 'rb') as f:
   circles = pickle.load(f)





all_locs=df1['origin_data'].values.tolist()+df1['destination_data'].values.tolist()
all_locs=sorted(list(set(all_locs)))

circles=[]
for i in tqdm(all_locs,total=len(all_locs)):
    tmp1=df1[(df1['origin_data']==i)].reset_index()
    tmp2=df1[(df1['destination_data']==i)].reset_index()
    if len(tmp1)>1:
        circles.append([[tmp1['origin_lat'][0],tmp1['origin_lng'][0]],len(tmp1)+len(tmp2),i])
    elif len(tmp2)>1:
        circles.append([[tmp2['destination_lat'][0],tmp2['destination_lng'][0]],len(tmp1)+len(tmp2),i])
    else:
        continue









origin_st=['AL'
'AR'
'AZ'
'CA']

df1=df[df['customer'].isin(customer)]

df1['created_year']=df1['created_year'].astype(str)
df1['created_week']=df1['created_week'].astype(str)
df1['year_month']=df1[['created_year', 'created_month']].agg(' '.join, axis=1)
df1['travel_data']=df1[['origin_data', 'destination_data']].agg('->'.join, axis=1)
df1['year_week_str']=df1[['created_year', 'created_week']].agg(' Week '.join, axis=1)
df1['created_year']=df1['created_year'].astype(int)
df1['created_week']=df1['created_week'].astype(int)
carr=df1.groupby([selector0,selector]).agg({'id':'count',
											'year_week':'max',
											'created_year':'max',
											'created_week':'max',
											'created_month_num':'max'}).reset_index()
if selector=='year_week_str':
	carr.sort_values(by=['created_year','created_week'],ascending=True,inplace=True)
else:
	carr.sort_values(by=['created_year','created_month_num'],ascending=True,inplace=True)

if len(carr)<5:
	return JsonResponse({"error":[True]})
lanes=carr.groupby([selector0]).count().sort_values(by=['id'],
				ascending=False)['id'][:20].index.tolist()
carr=carr[carr[selector0].isin(lanes)]
year_weeks=carr[selector].unique().tolist()
if selector=='year_week_str':
	year_weeks=year_weeks[-14:]
# else:
# 	year_weeks.sort()
heats=[]
for i in lanes:
	vals=[]
	for j in year_weeks:
		try:
			val=carr[(carr[selector]==j) &\
					(carr[selector0]==i)]['id'].values.tolist()[0]
		except IndexError as e:
			val=0
		vals.append(val)
	heats.append({
		"name": i,
		"data": vals
		})

ranges=[]
lst = range(1,carr['id'].max()+1)
nameheat=['low','medium','high','extreme']
colorheat=['#00A100','#128FD9','#FFB200','#FF0000']
for i in zip(np.array_split(lst, 4),nameheat,colorheat):
	ranges.append(
			{
	"from": int(i[0].min()),
	"to": int(i[0].max()),
	"name": i[1],
	"color": i[2]
})
















