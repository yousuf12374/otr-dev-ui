#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 25 14:46:20 2021

@author: yousuf
"""
from tqdm import tqdm

import pandas as pd
FL=["FL_trucks_2021_01.csv","FL_trucks_2021_02.csv"]
CA=["CA_trucks_2021_01.csv","CA_trucks_2021_02.csv"]
IL=["IL_trucks_2021_01.csv","IL_trucks_2021_02.csv"]
NE=["NE_trucks_2021_01.csv","NE_trucks_2021_02.csv"]
omaha=["omaha_trucks_2021_01.csv","omaha_trucks_2021_02.csv"]
dirz="/home/yousuf/Downloads/scalecapacity/text_proj/trucks/"

trucks_dtype={
			                   'created_week': int,
                              'duration': float,
                              'asset_equipment_origin_namedCoordinates_latitude': float,
                              'asset_equipment_origin_namedCoordinates_longitude': float,
                              'asset_equipment_destination_place_namedCoordinates_latitude': float,
                              'asset_equipment_destination_place_namedCoordinates_longitude': float,
                              'asset_equipment_origin_namedCoordinates_stateProvince': str,
                              'asset_equipment_origin_namedCoordinates_city': str,
                              'asset_equipment_destination_place_namedCoordinates_stateProvince': str,
                              'asset_equipment_destination_place_namedCoordinates_city': str,
                              'asset_equipment_equipmentType': str,
                              'asset_assetId': str,
                              'callback_userId': str,
                              'callback_companyName': str,
			                     'destination_data': str,
                              'travel_data': str,
			                     'origin_data': str,
                              'created_at': str
			                 }

dct={}
dct['FL']=FL
dct['CA']=CA
dct['IL']=IL
dct['NE']=NE
dct['omaha']=omaha
all_frames=[]
for j in tqdm(dct.items(),total=len(dct)):
    frames=[]
    for i in j[1]:
        
        df1 = pd.read_csv(dirz+i,
    				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
    			                 dtype={
    			                     'callback_userId': str,
    			                     'creditScore_score': float,
    			                     'asset_dimensions_weightPounds': float
    			                 })
        frames.append(df1)
    df=pd.concat(frames)
    df['origin_data'] = df[['asset_equipment_origin_namedCoordinates_stateProvince','asset_equipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
    
    df['destination_data'] = df[['asset_equipment_destination_place_namedCoordinates_stateProvince','asset_equipment_destination_place_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
    
    df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)
    
    df['origin_data'] = df['origin_data'].str.upper()
    df['destination_data'] = df['destination_data'].str.upper()
    
    df['travel_data'] = df['travel_data'].str.upper()
    
    df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)
    
    df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))
    
    df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)
    
    df=df.fillna(0)
    df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
    df['created_week']=df['created_week'].astype(int)
    df['created_year']=df['asset_status_created_date'].apply(lambda x : x.strftime("%Y"))
    df['created_year']=df['created_year'].astype(int)
    df=df[df['created_year']==2021]
    # df=df[df['created_week']<=8]
    clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
          'asset_equipment_equipmentType',
           'asset_equipment_origin_namedCoordinates_stateProvince',
           'asset_equipment_origin_namedCoordinates_city',
           'asset_equipment_origin_namedCoordinates_latitude',
           'asset_equipment_origin_namedCoordinates_longitude',
           'asset_equipment_destination_place_namedCoordinates_stateProvince',
           'asset_equipment_destination_place_namedCoordinates_city',
           'asset_equipment_destination_place_namedCoordinates_latitude',
           'asset_equipment_destination_place_namedCoordinates_longitude', 'origin_data',
           'destination_data', 'travel_data', 'duration', 'created_at',
           'created_week']
    df=df[clss]
    all_frames.append(df)

df=pd.concat(all_frames)
df.drop_duplicates(subset=['asset_assetId', 'callback_userId'], keep='last')
df.to_csv(dirz+"MAP_trucks.csv",index=0)































# df1 = pd.read_csv(dirz+IL[0],
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })
# df2 = pd.read_csv(dirz+IL[1],
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })

# df=pd.concat([df1,df2])

# df['origin_data'] = df[['asset_equipment_origin_namedCoordinates_stateProvince','asset_equipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['destination_data'] = df[['asset_equipment_destination_place_namedCoordinates_stateProvince','asset_equipment_destination_place_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)

# df['origin_data'] = df['origin_data'].str.upper()
# df['destination_data'] = df['destination_data'].str.upper()

# df['travel_data'] = df['travel_data'].str.upper()

# df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)

# df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))

# df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)

# df=df.fillna(0)
# df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
# df['created_week']=df['created_week'].astype(int)
# df=df[df['created_week']<=8]

# clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
#       'asset_equipment_equipmentType',
#        'asset_equipment_origin_namedCoordinates_stateProvince',
#        'asset_equipment_origin_namedCoordinates_city',
#        'asset_equipment_origin_namedCoordinates_latitude',
#        'asset_equipment_origin_namedCoordinates_longitude',
#        'asset_equipment_destination_place_namedCoordinates_stateProvince',
#        'asset_equipment_destination_place_namedCoordinates_city',
#        'asset_equipment_destination_place_namedCoordinates_latitude',
#        'asset_equipment_destination_place_namedCoordinates_longitude', 'origin_data',
#        'destination_data', 'travel_data', 'duration', 'created_at',
#        'created_week']
# df=df[clss]
# df.to_csv(dirz+"MAP_IL_trucks.csv",index=0)











# # df = pd.read_csv(dirz+"MAP_NE_OMAHA_trucks.csv",
# # 				parse_dates=['asset_status_created_date'],
# # 			                 dtype=trucks_dtype)





# df1 = pd.read_csv(dirz+"MAP_NE_trucks.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=trucks_dtype)

# df2 = pd.read_csv(dirz+"MAP_FL_trucks.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=trucks_dtype)

# df3 = pd.read_csv(dirz+"MAP_IL_trucks.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=trucks_dtype)

# df4 = pd.read_csv(dirz+"MAP_CA_trucks.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=trucks_dtype)

# df=pd.concat([df1,df2,df3,df4])

# df.to_csv(dirz+"MAP_trucks.csv",index=0)















































