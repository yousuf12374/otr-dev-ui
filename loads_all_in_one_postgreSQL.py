#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 19:06:10 2021

@author: yousuf
"""

from tqdm import tqdm
import pandas as pd
import os
dirz="/home/yousuf/Downloads/scalecapacity/text_proj/loads_new/"
loads_dtype={
			                     'created_week': int,
          'duration': float,
          'asset_shipment_origin_namedCoordinates_latitude': float,
          'asset_shipment_origin_namedCoordinates_longitude': float,
          'asset_shipment_destination_namedCoordinates_latitude': float,
          'asset_shipment_destination_namedCoordinates_longitude': float,
          'asset_shipment_origin_namedCoordinates_stateProvince': str,
          'asset_shipment_origin_namedCoordinates_city': str,
          'asset_shipment_destination_namedCoordinates_stateProvince': str,
          'asset_shipment_destination_namedCoordinates_city': str,
          'asset_shipment_equipmentType': str,
          'asset_assetId': str,
          'callback_userId': str,
          'callback_companyName': str,
			                     'destination_data': str,
          'travel_data': str,
			                     'origin_data': str,
          'created_at': str
			                 }
loads=[x for x in os.listdir(dirz) if x.endswith("csv")]

zone_df=pd.read_csv("/home/yousuf/Downloads/scalecapacity/us-zip-code-latitude-and-longitude.csv",delimiter=";",dtype={'Zip':str})

zone_df['location'] = zone_df[['State','City']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df['location'] = zone_df['location'].str.upper()

zone_df1=pd.read_csv("/home/yousuf/Downloads/scalecapacity/simplemaps_uszips_basicv1.77/uszips.csv",dtype={'county_fips':str})

zone_df1['location'] = zone_df1[['state_id','city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df1['location'] = zone_df1['location'].str.upper()

zone_df2=pd.read_csv("/home/yousuf/Downloads/scalecapacity/zipcodes-csv-10-Aug-2004/zipcode.csv",dtype={'zip':str})

zone_df2['location'] = zone_df2[['state','city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df2['location'] = zone_df2['location'].str.upper()



dct={}
for i in zone_df.itertuples():
    dct[i[9]]=i[1][0]
for i in zone_df1.itertuples():
    dct[i[19]]=i[11][0]
for i in zone_df2.itertuples():
    dct[i[8]]=i[1][0]
    
for i in tqdm(loads,total=len(loads)):
    # tqdm.pandas()
    break
    df = pd.read_csv(dirz+i,
				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
			                 dtype={
			                     'callback_userId': str,
			                     'creditScore_score': float,
			                     'asset_dimensions_weightPounds': float
			                 })
    df['origin_data'] = df[['asset_shipment_origin_namedCoordinates_stateProvince','asset_shipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
    
    df['destination_data'] = df[['asset_shipment_destination_namedCoordinates_stateProvince','asset_shipment_destination_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
    
    df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)
    
    df['origin_data'] = df['origin_data'].str.upper()    
    
    df['origin_zone'] = df[['origin_data','callback_userId']].apply(lambda row: dct.get(row['origin_data'],'x'), axis=1)

    df['destination_data'] = df['destination_data'].str.upper()
    
    df['destination_zone'] = df[['destination_data','callback_userId']].apply(lambda row: dct.get(row['destination_data'],'x'), axis=1)

    df['travel_data'] = df['travel_data'].str.upper()
    
    df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)
    
    df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))
    
    df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)
    
    df=df.fillna(0)
    df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
    df['created_week']=df['created_week'].astype(int)
    df['created_year']=df['asset_status_created_date'].apply(lambda x : x.strftime("%Y"))
    df['created_year']=df['created_year'].astype(int)
    df=df[df['created_year']==2021]
    
    # df=df[df['created_week']<=10]
    clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
          'asset_status_endDate','asset_status_startDate',
            'asset_shipment_equipmentType',
            'asset_shipment_origin_namedCoordinates_stateProvince',
            'asset_shipment_origin_namedCoordinates_city',
            'asset_shipment_origin_namedCoordinates_latitude',
            'asset_shipment_origin_namedCoordinates_longitude',
            'asset_shipment_destination_namedCoordinates_stateProvince',
            'asset_shipment_destination_namedCoordinates_city',
            'asset_shipment_destination_namedCoordinates_latitude',
            'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
            'destination_data', 'travel_data', 'duration', 'created_at',
            'created_week','origin_zone','destination_zone']
    df=df[clss]
    df.drop_duplicates(subset=['asset_assetId', 'callback_userId'], keep='last')
    df.to_csv(dirz+i,index=0)
    

post_sql=True

if post_sql:
    from tqdm import tqdm
    import pandas as pd
    import os
    loads_dtype={
             'created_week': int,
              'duration': float,
              'asset_shipment_origin_namedCoordinates_latitude': float,
              'asset_shipment_origin_namedCoordinates_longitude': float,
              'asset_shipment_destination_namedCoordinates_latitude': float,
              'asset_shipment_destination_namedCoordinates_longitude': float,
              'asset_shipment_origin_namedCoordinates_stateProvince': str,
              'asset_shipment_origin_namedCoordinates_city': str,
              'asset_shipment_destination_namedCoordinates_stateProvince': str,
              'asset_shipment_destination_namedCoordinates_city': str,
              'asset_assetId': str,
              'callback_userId': str,
              'callback_companyName': str,
             'destination_data': str,
              'travel_data': str,
             'origin_data': str,
              'created_at': str,
              'origin_zone': str,
              'destination_zone': str
         }
    trucks_dtype={
           'created_week': int,
          'duration': float,
          'asset_equipment_origin_namedCoordinates_latitude': float,
          'asset_equipment_origin_namedCoordinates_longitude': float,
          'asset_equipment_destination_place_namedCoordinates_latitude': float,
          'asset_equipment_destination_place_namedCoordinates_longitude': float,
          'asset_equipment_origin_namedCoordinates_stateProvince': str,
          'asset_equipment_origin_namedCoordinates_city': str,
          'asset_equipment_destination_place_namedCoordinates_stateProvince': str,
          'asset_equipment_destination_place_namedCoordinates_city': str,
          'asset_assetId': str,
          'callback_userId': str,
          'callback_companyName': str,
             'destination_data': str,
          'travel_data': str,
             'origin_data': str,
          'created_at': str,
        'origin_zone': str,
        'destination_zone': str
         }
    clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
            'asset_shipment_equipmentType',
            'asset_shipment_origin_namedCoordinates_stateProvince',
            'asset_shipment_origin_namedCoordinates_city',
            'asset_shipment_origin_namedCoordinates_latitude',
            'asset_shipment_origin_namedCoordinates_longitude',
            'asset_shipment_destination_namedCoordinates_stateProvince',
            'asset_shipment_destination_namedCoordinates_city',
            'asset_shipment_destination_namedCoordinates_latitude',
            'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
            'destination_data', 'travel_data', 'duration', 'created_at',
            'created_week','origin_zone','destination_zone']
    # dirz=f"{os.getcwd()}/loads/"
    # df_load = pd.read_csv(dirz+"MAP_loads.csv",
    # 		parse_dates=['asset_status_created_date'],
    # 	                 dtype=loads_dtype)
    # dirz=f"{os.getcwd()}/trucks/"
    # df_truck = pd.read_csv(dirz+"MAP_trucks.csv",
    # 				parse_dates=['asset_status_created_date'],
    # 			                 dtype=trucks_dtype)
    import psycopg2
    from psycopg2 import Error
    # from sqlite3 import Error
    # import sqlite3 as lite
    import os
    # dbname=f'{os.getcwd()}/db.sqlite3'
    # conn = lite.connect(dbname)
    conn = psycopg2.connect(user="otr_just_maps",
                                  password="otrdev123321",
                                  host="otr.ciwoso6g1uql.us-east-1.rds.amazonaws.com",
                                  port="5432",
                                  database="postgres")
    
    # Create a cursor to perform database operations
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS loads (
                  id SERIAL PRIMARY KEY,
                  asset_assetId varchar(255) NOT NULL UNIQUE,
                  callback_userId varchar(255) NOT NULL,
                  callback_companyName varchar(255) NOT NULL,
                  asset_status_created_date varchar(255) NOT NULL,
                  asset_shipment_equipmentType varchar(255) NOT NULL,
                  asset_shipment_origin_namedCoordinates_stateProvince varchar(255) NOT NULL,
                  asset_shipment_origin_namedCoordinates_city varchar(255) NOT NULL,
                  asset_shipment_origin_namedCoordinates_latitude varchar(255) NOT NULL,
                  asset_shipment_origin_namedCoordinates_longitude varchar(255) NOT NULL,
                  asset_shipment_destination_namedCoordinates_stateProvince varchar(255) NOT NULL,
                  asset_shipment_destination_namedCoordinates_city varchar(255) NOT NULL,
                  asset_shipment_destination_namedCoordinates_latitude varchar(255) NOT NULL,
                  asset_shipment_destination_namedCoordinates_longitude varchar(255) NOT NULL,
                  origin_data varchar(255) NOT NULL,
                  destination_data varchar(255) NOT NULL,
                  duration varchar(255) NOT NULL,
                  created_at varchar(255) NOT NULL,
                  created_week varchar(255) NOT NULL,
                  origin_zone varchar(255) NOT NULL,
                  destination_zone varchar(255) NOT NULL,
                  origin_geom GEOMETRY DEFAULT NULL,
                  destination_geom GEOMETRY DEFAULT NULL
                  );""")
    conn.commit()
    for k in tqdm(loads,total=len(loads)):
        df = pd.read_csv(dirz+k,
        		parse_dates=['asset_status_created_date'],
        	                 dtype=loads_dtype)
        df=df[clss]
        for i in tqdm(df.itertuples(),total=len(df)):
            postgresql=f"""INSERT INTO loads (asset_assetId, callback_userId, callback_companyName,\
                 asset_status_created_date, asset_shipment_equipmentType,\
            asset_shipment_origin_namedCoordinates_stateProvince, asset_shipment_origin_namedCoordinates_city,
            asset_shipment_origin_namedCoordinates_latitude, asset_shipment_origin_namedCoordinates_longitude,
            asset_shipment_destination_namedCoordinates_stateProvince, asset_shipment_destination_namedCoordinates_city,
            asset_shipment_destination_namedCoordinates_latitude, asset_shipment_destination_namedCoordinates_longitude,
            origin_data, destination_data,duration,created_at,created_week,origin_zone,destination_zone, origin_geom,\
              destination_geom  ) values (
            '{i[1]}', '{i[2]}', '{i[3].replace("'",'')}', '{str(i[4])}', 
            '{i[5]}', '{i[6]}', '{i[7]}', '{str(i[8])}', 
            '{str(i[9])}', '{str(i[10])}', '{str(i[11])}', '{str(i[12])}', 
            '{str(i[13])}', '{i[14]}', '{i[15]}',
            '{str(i[17])}', '{str(i[18])}', '{i[19]}', '{i[20]}', '{i[21]}', \
                ST_GeomFromText('POINT({str(i[9])} {str(i[8])})',4326),\
                  ST_GeomFromText('POINT({str(i[13])} {str(i[12])})',4326) ) \
            ON CONFLICT (asset_assetId) DO UPDATE SET callback_userId='{i[2]}', asset_status_created_date='{str(i[4])}', 
            asset_shipment_origin_namedCoordinates_latitude='{str(i[8])}', asset_shipment_origin_namedCoordinates_longitude='{str(i[9])}', 
            asset_shipment_destination_namedCoordinates_latitude='{str(i[12])}', asset_shipment_destination_namedCoordinates_longitude='{str(i[13])}', 
            created_at='{str(i[18])}',origin_zone='{str(i[20])}',destination_zone='{str(i[21])}'; """
            cursor.execute(postgresql)
        conn.commit()
        \copy loads_new(asset_assetId, callback_userId, callback_companyName,asset_status_created_date, asset_shipment_equipmentType,asset_shipment_origin_namedCoordinates_stateProvince, asset_shipment_origin_namedCoordinates_city,asset_shipment_origin_namedCoordinates_latitude, asset_shipment_origin_namedCoordinates_longitude,asset_shipment_destination_namedCoordinates_stateProvince, asset_shipment_destination_namedCoordinates_city,asset_shipment_destination_namedCoordinates_latitude, asset_shipment_destination_namedCoordinates_longitude,origin_data, destination_data,duration,created_at,created_week,origin_zone,destination_zone) FROM '/home/yousuf/Downloads/scalecapacity/text_proj/loads_new/loads_2021_03_3.csv' delimiter ',' csv
    # cursor.execute("""CREATE TABLE IF NOT EXISTS trucks (
    #               id INTEGER PRIMARY KEY,
    #               asset_assetId TEXT NOT NULL UNIQUE,
    #               callback_userId TEXT NOT NULL,
    #               callback_companyName TEXT NOT NULL,
    #               asset_status_created_date TEXT NOT NULL,
    #               asset_equipment_equipmentType TEXT NOT NULL,
    #               asset_equipment_origin_namedCoordinates_stateProvince TEXT NOT NULL,
    #               asset_equipment_origin_namedCoordinates_city TEXT NOT NULL,
    #               asset_equipment_origin_namedCoordinates_latitude TEXT NOT NULL,
    #               asset_equipment_origin_namedCoordinates_longitude TEXT NOT NULL,
    #               asset_equipment_destination_place_namedCoordinates_stateProvince TEXT NOT NULL,
    #               asset_equipment_destination_place_namedCoordinates_city TEXT NOT NULL,
    #               asset_equipment_destination_place_namedCoordinates_latitude TEXT NOT NULL,
    #               asset_equipment_destination_place_namedCoordinates_longitude TEXT NOT NULL,
    #               origin_data TEXT NOT NULL,
    #               destination_data TEXT NOT NULL,
    #               travel_data TEXT NOT NULL,
    #               duration TEXT NOT NULL,
    #               created_at TEXT NOT NULL,
    #               created_week TEXT NOT NULL
    #               );""")
    # conn.commit()
    
    # df=df_truck.copy()
    # for i in tqdm(df.itertuples(),total=len(df)):
    #     postgresql=f"""INSERT INTO trucks (asset_assetId, callback_userId, callback_companyName, asset_status_created_date, asset_equipment_equipmentType,
    #     asset_equipment_origin_namedCoordinates_stateProvince, asset_equipment_origin_namedCoordinates_city,
    #     asset_equipment_origin_namedCoordinates_latitude, asset_equipment_origin_namedCoordinates_longitude,
    #     asset_equipment_destination_place_namedCoordinates_stateProvince, asset_equipment_destination_place_namedCoordinates_city,
    #     asset_equipment_destination_place_namedCoordinates_latitude, asset_equipment_destination_place_namedCoordinates_longitude,
    #     origin_data, destination_data,travel_data,duration,created_at,created_week) values (
    #     "{i[1]}", "{i[2]}", "{i[3]}", "{str(i[4])}", 
    #     "{i[5]}", "{i[6]}", "{i[7]}", "{str(i[8])}", 
    #     "{str(i[9])}", "{str(i[10])}", "{str(i[11])}", "{str(i[12])}", 
    #     "{str(i[13])}", "{i[14]}", "{i[15]}", "{i[16]}", 
    #     "{str(i[17])}", "{str(i[18])}", "{i[19]}") 
    #     ON CONFLICT (asset_assetId) DO UPDATE SET callback_userId="{i[2]}", asset_status_created_date="{str(i[4])}", 
    #     asset_equipment_origin_namedCoordinates_latitude="{str(i[8])}", asset_equipment_origin_namedCoordinates_longitude="{str(i[9])}", 
    #     asset_equipment_destination_place_namedCoordinates_latitude="{str(i[12])}", asset_equipment_destination_place_namedCoordinates_longitude="{str(i[13])}", 
    #     created_at="{str(i[18])}"; """
    #     cursor.execute(postgresql)
    #     break
    # conn.commit()
    cursor.close()
    conn.commit()
    conn.close()















































































