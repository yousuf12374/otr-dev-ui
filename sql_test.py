#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  1 02:04:39 2021

@author: yousuf
"""

from tqdm import tqdm
import pandas as pd
import os
loads_dtype={
         'created_week': int,
          'duration': float,
          'asset_shipment_origin_namedCoordinates_latitude': float,
          'asset_shipment_origin_namedCoordinates_longitude': float,
          'asset_shipment_destination_namedCoordinates_latitude': float,
          'asset_shipment_destination_namedCoordinates_longitude': float,
          'asset_shipment_origin_namedCoordinates_stateProvince': str,
          'asset_shipment_origin_namedCoordinates_city': str,
          'asset_shipment_destination_namedCoordinates_stateProvince': str,
          'asset_shipment_destination_namedCoordinates_city': str,
          'asset_assetId': str,
          'callback_userId': str,
          'callback_companyName': str,
         'destination_data': str,
          'travel_data': str,
         'origin_data': str,
          'created_at': str,
          'origin_zone': str,
          'destination_zone': str
     }
trucks_dtype={
       'created_week': int,
      'duration': float,
      'asset_equipment_origin_namedCoordinates_latitude': float,
      'asset_equipment_origin_namedCoordinates_longitude': float,
      'asset_equipment_destination_place_namedCoordinates_latitude': float,
      'asset_equipment_destination_place_namedCoordinates_longitude': float,
      'asset_equipment_origin_namedCoordinates_stateProvince': str,
      'asset_equipment_origin_namedCoordinates_city': str,
      'asset_equipment_destination_place_namedCoordinates_stateProvince': str,
      'asset_equipment_destination_place_namedCoordinates_city': str,
      'asset_assetId': str,
      'callback_userId': str,
      'callback_companyName': str,
         'destination_data': str,
      'travel_data': str,
         'origin_data': str,
      'created_at': str,
    'origin_zone': str,
    'destination_zone': str
     }
clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
        'asset_shipment_equipmentType',
        'asset_shipment_origin_namedCoordinates_stateProvince',
        'asset_shipment_origin_namedCoordinates_city',
        'asset_shipment_origin_namedCoordinates_latitude',
        'asset_shipment_origin_namedCoordinates_longitude',
        'asset_shipment_destination_namedCoordinates_stateProvince',
        'asset_shipment_destination_namedCoordinates_city',
        'asset_shipment_destination_namedCoordinates_latitude',
        'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
        'destination_data', 'travel_data', 'duration', 'created_at',
        'created_week','origin_zone','destination_zone']
# dirz=f"{os.getcwd()}/loads/"
# df_load = pd.read_csv(dirz+"MAP_loads.csv",
# 		parse_dates=['asset_status_created_date'],
# 	                 dtype=loads_dtype)
# dirz=f"{os.getcwd()}/trucks/"
# df_truck = pd.read_csv(dirz+"MAP_trucks.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=trucks_dtype)

from sqlite3 import Error
import sqlite3 as lite
import os
dbname=f'{os.getcwd()}/db.sqlite3'
conn = lite.connect(dbname)
# cursor = conn.cursor()
import time
start=time.time()
df=pd.read_sql("SELECT asset_assetId,\
			asset_shipment_destination_namedCoordinates_latitude,\
			asset_shipment_destination_namedCoordinates_longitude,\
			asset_shipment_origin_namedCoordinates_latitude,\
			asset_shipment_origin_namedCoordinates_longitude,origin_zone,destination_zone\
		 FROM loads where created_month='April' and origin_zone!='x' and destination_zone!='x' LIMIT 10000",con=conn)
print(f"took: [{int(time.time()-start)}] seconds")
cols=df['origin_zone'].astype(int).unique().tolist()
cols.sort()
df1=pd.DataFrame(columns=["Zone "+str(x) for x in cols])
for i in tqdm(cols,total=len(cols)):
    vals=[]
    for j in cols:
        cnts=df[(df['destination_zone']==str(j)) & (df['origin_zone']==str(i))].count().max()
        vals.append(cnts)
    df1.loc["Zone "+str(i)] = vals


df=pd.read_sql("SELECT asset_assetId,\
			asset_shipment_destination_namedCoordinates_latitude,\
			asset_shipment_destination_namedCoordinates_longitude,\
			asset_shipment_origin_namedCoordinates_latitude,\
			asset_shipment_origin_namedCoordinates_longitude,origin_zone,destination_zone\
		 FROM loads where created_month='March' and origin_zone!='x' and destination_zone!='x' LIMIT 10000",con=conn)
print(f"took: [{int(time.time()-start)}] seconds")
cols=df['origin_zone'].astype(int).unique().tolist()
cols.sort()
df2=pd.DataFrame(columns=["Zone "+str(x) for x in cols])
for i in tqdm(cols,total=len(cols)):
    vals=[]
    for j in cols:
        cnts=df[(df['destination_zone']==str(j)) & (df['origin_zone']==str(i))].count().max()
        vals.append(cnts)
    df2.loc["Zone "+str(i)] = vals


df_compare=df1.subtract(df2)
df3=df1.replace(0, 1)
dfc=df_compare.div(df3)
dfc=dfc*100
dfc=dfc.astype(int)

values=[["Zone "+str(x) for x in cols]]
[values.append(x) for x in dfc.values.tolist()]

import plotly.graph_objects as go

import pandas as pd

fig = go.Figure(data=[go.Table(
  header=dict(
    values=['From']+list(df_compare.columns),
    line_color='white', fill_color='white',
    align='center', font=dict(color='black', size=12)
  ),
  cells=dict(
    values=values,
    # line_color=[df_compare.Color], fill_color=[df_compare['Zone 1']],
    align='center', font=dict(color='black', size=11)
  ))
])

# fig.show()
fig.write_html("file.html")

# cursor.close()
conn.close()