#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 17:32:54 2021

@author: yousuf
"""

from tqdm import tqdm
import pandas as pd
import os
dirz="/home/yousuf/Downloads/scalecapacity/text_proj/loads/"
loads_dtype={
			                     'created_week': int,
                                  'duration': float,
                                  'asset_shipment_origin_namedCoordinates_latitude': float,
                                  'asset_shipment_origin_namedCoordinates_longitude': float,
                                  'asset_shipment_destination_namedCoordinates_latitude': float,
                                  'asset_shipment_destination_namedCoordinates_longitude': float,
                                  'asset_shipment_origin_namedCoordinates_stateProvince': str,
                                  'asset_shipment_origin_namedCoordinates_city': str,
                                  'asset_shipment_destination_namedCoordinates_stateProvince': str,
                                  'asset_shipment_destination_namedCoordinates_city': str,
                                  'asset_shipment_equipmentType': str,
                                  'asset_assetId': str,
                                  'callback_userId': str,
                                  'callback_companyName': str,
			                     'destination_data': str,
                                  'travel_data': str,
			                     'origin_data': str,
                                  'created_at': str
			                 }


zone_df=pd.read_csv("/home/yousuf/Downloads/scalecapacity/us-zip-code-latitude-and-longitude.csv",delimiter=";",dtype={'Zip':str})


zone_df['location'] = zone_df[['State','City']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

zone_df['location'] = zone_df['location'].str.upper()

def loc_zip(row,typ):
    try:
        return str(zone_df[zone_df['location']==row[typ]]['Zip'].max())
    except:
        return 'x' 


def loc_zone(row,typ):
    try:
        zip_=str(zone_df[zone_df['location']==row[typ]]['Zip'].max())
        return str(zip_[0])
    except:
        return 'x'

df['origin_zip'] = df[['origin_data','callback_userId']].apply(lambda row: loc_zip(row,'origin_data'), axis=1)

df['origin_zone'] = df[['origin_data','callback_userId']].apply(lambda row: loc_zip(row,'origin_data'), axis=1)



loads=os.listdir()

for i in loads:
    df = pd.read_csv(dirz+i,
				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
			                 dtype={
			                     'callback_userId': str,
			                     'creditScore_score': float,
			                     'asset_dimensions_weightPounds': float
			                 })
df['origin_data'] = df[['asset_shipment_origin_namedCoordinates_stateProvince','asset_shipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

df['destination_data'] = df[['asset_shipment_destination_namedCoordinates_stateProvince','asset_shipment_destination_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)

df['origin_data'] = df['origin_data'].str.upper()

df['destination_data'] = df['destination_data'].str.upper()

df['travel_data'] = df['travel_data'].str.upper()

df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)

df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))

df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)

df=df.fillna(0)
df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
df['created_week']=df['created_week'].astype(int)
df['created_year']=df['asset_status_created_date'].apply(lambda x : x.strftime("%Y"))
df['created_year']=df['created_year'].astype(int)
df=df[df['created_year']==2021]
# df=df[df['created_week']<=10]
clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
        'asset_shipment_equipmentType',
        'asset_shipment_origin_namedCoordinates_stateProvince',
        'asset_shipment_origin_namedCoordinates_city',
        'asset_shipment_origin_namedCoordinates_latitude',
        'asset_shipment_origin_namedCoordinates_longitude',
        'asset_shipment_destination_namedCoordinates_stateProvince',
        'asset_shipment_destination_namedCoordinates_city',
        'asset_shipment_destination_namedCoordinates_latitude',
        'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
        'destination_data', 'travel_data', 'duration', 'created_at',
        'created_week']
df=df[clss]
df.drop_duplicates(subset=['asset_assetId', 'callback_userId'], keep='last')
df.to_csv(dirz+"MAP_loads.csv",index=0)




















































































