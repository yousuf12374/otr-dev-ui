# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# """
# Created on Mon Apr 26 13:04:11 2021

# @author: yousuf
# """
# # import ray
# # ray.init()
# # import modin.pandas as pd
# import pandas as pd
# loads_dtype={
#          'created_week': int,
#           'duration': float,
#           'asset_shipment_origin_namedCoordinates_latitude': float,
#           'asset_shipment_origin_namedCoordinates_longitude': float,
#           'asset_shipment_destination_namedCoordinates_latitude': float,
#           'asset_shipment_destination_namedCoordinates_longitude': float,
#           'asset_shipment_origin_namedCoordinates_stateProvince': str,
#           'asset_shipment_origin_namedCoordinates_city': str,
#           'asset_shipment_destination_namedCoordinates_stateProvince': str,
#           'asset_shipment_destination_namedCoordinates_city': str,
#           'asset_assetId': str,
#           'callback_userId': str,
#           'callback_companyName': str,
#          'destination_data': str,
#           'travel_data': str,
#          'origin_data': str,
#           'created_at': str
#      }
# trucks_dtype={
#        'created_week': int,
#       'duration': float,
#       'asset_equipment_origin_namedCoordinates_latitude': float,
#       'asset_equipment_origin_namedCoordinates_longitude': float,
#       'asset_equipment_destination_place_namedCoordinates_latitude': float,
#       'asset_equipment_destination_place_namedCoordinates_longitude': float,
#       'asset_equipment_origin_namedCoordinates_stateProvince': str,
#       'asset_equipment_origin_namedCoordinates_city': str,
#       'asset_equipment_destination_place_namedCoordinates_stateProvince': str,
#       'asset_equipment_destination_place_namedCoordinates_city': str,
#       'asset_assetId': str,
#       'callback_userId': str,
#       'callback_companyName': str,
#          'destination_data': str,
#       'travel_data': str,
#          'origin_data': str,
#       'created_at': str
#      }

# dirz="/home/yousuf/Downloads/scalecapacity/text_proj/loads/"
# df1 = pd.read_csv(dirz+"MAP_loads.csv",
# 		parse_dates=['asset_status_created_date'],
# 	                 dtype=loads_dtype)

# dirz="/home/yousuf/Downloads/scalecapacity/text_proj/trucks/"
# df2 = pd.read_csv(dirz+"MAP_trucks.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=trucks_dtype)

# companies=['Armstrong Transport Group Inc','Viking Transportation Company Llc']
# carriers=['Takhar Transport Services','Narwal Trucking']
# lanes=['NE YORK->UT OGDEN','NE LINCOLN->TX TERRELL']

# df_1=df1[df1['callback_companyName'].isin(companies)]
# df_2=df1[df1['travel_data'].isin(lanes)]
# df_3=df2[df2['callback_companyName'].isin(carriers)]

# df=pd.concat([df_1,df_2])
# df_1=df_1.groupby(['callback_companyName','travel_data']).agg({"asset_assetId":"count",
# 	                                                             "asset_shipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_shipment_equipmentType']].reset_index()
# df_2=df_2.groupby(['callback_companyName','travel_data']).agg({"asset_assetId":"count",
# 	                                                             "asset_shipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_shipment_equipmentType']].reset_index()


# nodes = []
# nodes_list = []
# edges = []
# edges_list = []
# routes_list=[]

# for i in companies:
#     df_tmp_=df_1[df_1['callback_companyName']==i]
#     df_tmp=df_tmp_[0:10]
#     for j in df_tmp.itertuples():
#         if not str(j[1]) in nodes_list:
#             dct = {}
#             dct['id'] = "_".join(str(j[1]).split(" "))
#             dct['shape'] = 'image'
#             dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1622/1622593.svg'
#             dct['value'] = str(len(df_tmp_))
#             dct['title'] = str(j[1])
#             # dct['color']='#FF4500'
#             nodes_list.append(str(j[1]))
#             nodes.append(dct)
#         if not str(j[2]) in nodes_list:
#             dct = {}
#             dct['id'] = "_".join(str(j[2]).split(" "))
#             dct['shape'] = 'image'
#             dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1514/1514042.svg'
#             dct['value'] = str(len(df_tmp_))
#             dct['title'] = str(j[2])
#             # dct['color']='#FF4500'
#             nodes_list.append(str(j[2]))
#             routes_list.append(str(j[2]))
#             nodes.append(dct)
#         if not (str(j[1]),str(j[2])) in edges_list:
#             dct={}
#             dct['from'] = "_".join(str(j[1]).split(" "))
#             dct['to'] = "_".join(str(j[2]).split(" "))
#             dct['value']=str(j[3])
#             dct['title']=str(j[4])
#             edges_list.append((str(j[1]),str(j[2])))
#             edges.append(dct)    

# origin_list=[x.split('->')[0] for x in routes_list]

# df_4=df1[df1['travel_data'].isin(routes_list)]
# df_4=df_4[~df_4['callback_companyName'].isin(companies)]
# df_4=df_4.groupby(['callback_companyName','travel_data']).agg({"asset_assetId":"count",
# 	                                                             "asset_shipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_shipment_equipmentType']].reset_index()
# for i in df['travel_data'].unique().tolist():
#     df_tmp_=df_4[df_4['travel_data']==i]
#     df_tmp=df_tmp_[0:2]
#     break
#     for j in df_tmp.itertuples():
#         if not str(j[1]) in nodes_list:
#             dct = {}
#             dct['id'] = "_".join(str(j[1]).split(" "))
#             dct['shape'] = 'image'
#             dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1622/1622593.svg'
#             dct['value'] = str(len(df_tmp_))
#             dct['title'] = str(j[1])
#             # dct['color']='#FF4500'
#             nodes_list.append(str(j[1]))
#             nodes.append(dct)
#         if not str(j[2]) in nodes_list:
#             dct = {}
#             dct['id'] = "_".join(str(j[2]).split(" "))
#             dct['shape'] = 'image'
#             dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1514/1514042.svg'
#             dct['value'] = str(len(df_tmp_))
#             dct['title'] = str(j[2])
#             # dct['color']='#FF4500'
#             nodes_list.append(str(j[2]))
#             routes_list.append(str(j[2]))
#             nodes.append(dct)
#         if not (str(j[1]),str(j[2])) in edges_list:
#             dct={}
#             dct['from'] = "_".join(str(j[1]).split(" "))
#             dct['to'] = "_".join(str(j[2]).split(" "))
#             dct['value']=str(j[3])
#             dct['title']=str(j[4])
#             edges_list.append((str(j[1]),str(j[2])))
#             edges.append(dct)  

# for i in lanes:
#     df_tmp_=df_2[df_2['travel_data']==i]
#     df_tmp=df_tmp_[0:10]
#     for j in df_tmp.itertuples():
#         if not str(j[1]) in nodes_list:
#             dct = {}
#             dct['id'] = "_".join(str(j[1]).split(" "))
#             dct['shape'] = 'image'
#             dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1622/1622593.svg'
#             dct['value'] = str(len(df_tmp_))
#             dct['title'] = str(j[1])
#             # dct['color']='#FF4500'
#             nodes_list.append(str(j[1]))
#             nodes.append(dct)
#         if not str(j[2]) in nodes_list:
#             dct = {}
#             dct['id'] = "_".join(str(j[2]).split(" "))
#             dct['shape'] = 'image'
#             dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1514/1514042.svg'
#             dct['value'] = str(len(df_tmp_))
#             dct['title'] = str(j[2])
#             # dct['color']='#FF4500'
#             nodes_list.append(str(j[2]))
#             routes_list.append(str(j[2]))
#             nodes.append(dct)
#         if not (str(j[1]),str(j[2])) in edges_list:
#             dct={}
#             dct['from'] = "_".join(str(j[1]).split(" "))
#             dct['to'] = "_".join(str(j[2]).split(" "))
#             dct['value']=str(j[3])
#             dct['title']=str(j[4])
#             edges_list.append((str(j[1]),str(j[2])))
#             edges.append(dct)


# df_5=df_3.groupby(['callback_companyName','origin_data']).agg({"asset_assetId":"count",
# 	                                                             "asset_equipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_equipment_equipmentType']].reset_index()

# for j in df_5.itertuples():
#     if not str(j[1]) in nodes_list:
#         dct = {}
#         dct['id'] = "_".join(str(j[1]).split(" "))
#         dct['shape'] = 'image'
#         dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1622/1622593.svg'
#         dct['value'] = str(len(df_2[df_2['callback_companyName']==j[1]]))
#         dct['title'] = str(j[1])
#         # dct['color']='#FF4500'
#         nodes_list.append(str(j[1]))
#         nodes.append(dct)
#     r_l=[x for x in routes_list if j[2] in x]
#     for r in r_l:
#         if not (str(j[1]),str(r)) in edges_list:
#             dct={}
#             dct['from'] = "_".join(str(j[1]).split(" "))
#             dct['to'] = "_".join(str(r).split(" "))
#             dct['value']=str(j[3])
#             dct['title']=str(j[4])
#             edges_list.append((str(j[1]),str(r)))
#             edges.append(dct)      

# for x in lanes:
#     origin_list.append(x.split('->')[0])
# df_6=df2[df2['origin_data'].isin(origin_list)]
# df_7=df_6.groupby(['callback_companyName','origin_data']).agg({"asset_assetId":"count",
# 	                                                             "asset_equipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_equipment_equipmentType']].reset_index()
# df_7=df_7[0:10]
# for j in df_7.itertuples():
#     if not str(j[1]) in nodes_list:
#         dct = {}
#         dct['id'] = "_".join(str(j[1]).split(" "))
#         dct['shape'] = 'image'
#         dct['image'] = 'https://www.flaticon.com/premium-icon/icons/svg/1622/1622593.svg'
#         dct['value'] = str(len(df_2[df_2['callback_companyName']==j[1]]))
#         dct['title'] = str(j[1])
#         # dct['color']='#FF4500'
#         nodes_list.append(str(j[1]))
#         nodes.append(dct)
#     r_l=[x for x in routes_list if j[2] in x]
#     for r in r_l:
#         if not (str(j[1]),str(r)) in edges_list:
#             dct={}
#             dct['from'] = "_".join(str(j[1]).split(" "))
#             dct['to'] = "_".join(str(r).split(" "))
#             dct['value']=str(j[3])
#             dct['title']=str(j[4])
#             edges_list.append((str(j[1]),str(r)))
#             edges.append(dct)  


import pandas as pd
from tqdm import tqdm
loads_dtype={
         'created_week': int,
          'duration': float,
          'asset_shipment_origin_namedCoordinates_latitude': float,
          'asset_shipment_origin_namedCoordinates_longitude': float,
          'asset_shipment_destination_namedCoordinates_latitude': float,
          'asset_shipment_destination_namedCoordinates_longitude': float,
          'asset_shipment_origin_namedCoordinates_stateProvince': str,
          'asset_shipment_origin_namedCoordinates_city': str,
          'asset_shipment_destination_namedCoordinates_stateProvince': str,
          'asset_shipment_destination_namedCoordinates_city': str,
          'asset_assetId': str,
          'callback_userId': str,
          'callback_companyName': str,
         'destination_data': str,
          'travel_data': str,
         'origin_data': str,
          'created_at': str
     }
trucks_dtype={
       'created_week': int,
      'duration': float,
      'asset_equipment_origin_namedCoordinates_latitude': float,
      'asset_equipment_origin_namedCoordinates_longitude': float,
      'asset_equipment_destination_place_namedCoordinates_latitude': float,
      'asset_equipment_destination_place_namedCoordinates_longitude': float,
      'asset_equipment_origin_namedCoordinates_stateProvince': str,
      'asset_equipment_origin_namedCoordinates_city': str,
      'asset_equipment_destination_place_namedCoordinates_stateProvince': str,
      'asset_equipment_destination_place_namedCoordinates_city': str,
      'asset_assetId': str,
      'callback_userId': str,
      'callback_companyName': str,
         'destination_data': str,
      'travel_data': str,
         'origin_data': str,
      'created_at': str
     }

dirz="/home/yousuf/Downloads/scalecapacity/text_proj/loads/"
df1 = pd.read_csv(dirz+"MAP_loads.csv",
		parse_dates=['asset_status_created_date'],
	                 dtype=loads_dtype)

dirz="/home/yousuf/Downloads/scalecapacity/text_proj/trucks/"
df2 = pd.read_csv(dirz+"MAP_trucks.csv",
				parse_dates=['asset_status_created_date'],
			                 dtype=trucks_dtype)


df_1=df1
df_2=df1
df_3=df2

df=pd.concat([df_1,df_2])

cn=list(df1.groupby('callback_companyName').count()['callback_userId'].sort_values(ascending=False)[0:40].index)
cn=list(set(cn))
tn=list(df2.groupby('callback_companyName').count()['callback_userId'].sort_values(ascending=False)[0:40].index)
tn=list(set(tn))
td=list(df1.groupby('travel_data').count()['callback_userId'].sort_values(ascending=False)[0:40].index)
td=list(set(td))

df_1=df_1.groupby(['callback_companyName','travel_data']).agg({"asset_assetId":"count",
	                                                             "asset_shipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_shipment_equipmentType']].reset_index()
df_2=df_2.groupby(['callback_companyName','travel_data']).agg({"asset_assetId":"count",
	                                                             "asset_shipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_shipment_equipmentType']].reset_index()


nodes = []
nodes_list = []
edges = []
edges_list = []
routes_list=[]
for i in tqdm(cn,total=len(cn)):
    df_tmp_=df_1[df_1['callback_companyName']==i]
    df_tmp=df_tmp_[0:50]
    for j in df_tmp.itertuples():
        if not str(j[1]) in nodes_list:
            dct = {}
            dct['id'] = "_".join(str(j[1]).split(" "))
            dct['shape'] = 'image'
            dct['image'] = 'https://www.flaticon.com/svg/vstatic/svg/993/993928.svg?token=exp=1619447677~hmac=104a901d7d6c1d2f1c6914783a9062d7'
            dct['value'] = str(len(df_tmp_))
            dct['title'] = str(j[1])
            dct['label'] = str(j[1])
            # dct['color']='#FF4500'
            nodes_list.append(str(j[1]))
            nodes.append(dct)
        if not str(j[2]) in nodes_list:
            dct = {}
            dct['id'] = "_".join(str(j[2]).split(" "))
            dct['shape'] = 'image'
            dct['image'] = 'https://www.flaticon.com/svg/vstatic/svg/608/608691.svg?token=exp=1619447792~hmac=10b3312e6ecab924effbefe71987681e'
            dct['value'] = str(len(df_tmp_))
            dct['title'] = str(j[2])
            # dct['color']='#FF4500'
            nodes_list.append(str(j[2]))
            routes_list.append(str(j[2]))
            nodes.append(dct)
        if not (str(j[1]),str(j[2])) in edges_list:
            dct={}
            dct['from'] = "_".join(str(j[1]).split(" "))
            dct['to'] = "_".join(str(j[2]).split(" "))
            dct['value']=str(j[3])
            dct['title']=str(j[4])
            edges_list.append((str(j[1]),str(j[2])))
            edges.append(dct)    
origin_list=[x.split('->')[0] for x in routes_list]
df_4=df1[df1['travel_data'].isin(routes_list)]
df_4=df_4[~df_4['callback_companyName'].isin(cn)]
df_4=df_4.groupby(['callback_companyName','travel_data']).agg({"asset_assetId":"count",
	                                                             "asset_shipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_shipment_equipmentType']].reset_index()
for i in tqdm(td,total=len(td)):
    df_tmp_=df_4[df_4['travel_data']==i]
    df_tmp=df_tmp_[0:50]
    for j in df_tmp.itertuples():
        if not str(j[1]) in nodes_list:
            dct = {}
            dct['id'] = "_".join(str(j[1]).split(" "))
            dct['shape'] = 'image'
            dct['image'] = 'https://img.icons8.com/bubbles/100/000000/company.png'
            dct['value'] = str(len(df_tmp_))
            dct['title'] = str(j[1])
            dct['label'] = str(j[1])
            # dct['color']='#FF4500'
            nodes_list.append(str(j[1]))
            nodes.append(dct)
        if not str(j[2]) in nodes_list:
            dct = {}
            dct['id'] = "_".join(str(j[2]).split(" "))
            dct['shape'] = 'image'
            dct['image'] = 'https://www.flaticon.com/svg/vstatic/svg/608/608691.svg?token=exp=1619447792~hmac=10b3312e6ecab924effbefe71987681e'
            dct['value'] = str(len(df_tmp_))
            dct['title'] = str(j[2])
            # dct['color']='#FF4500'
            nodes_list.append(str(j[2]))
            routes_list.append(str(j[2]))
            nodes.append(dct)
        if not (str(j[1]),str(j[2])) in edges_list:
            dct={}
            dct['from'] = "_".join(str(j[1]).split(" "))
            dct['to'] = "_".join(str(j[2]).split(" "))
            dct['value']=str(j[3])
            dct['title']=str(j[4])
            edges_list.append((str(j[1]),str(j[2])))
            edges.append(dct)  

df_5=df_3.groupby(['callback_companyName','origin_data']).agg({"asset_assetId":"count",
	                                                             "asset_equipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_equipment_equipmentType']].reset_index()
df_5=df_5[0:50]
for j in tqdm(df_5.itertuples(),total=len(df_5)):
    if not str(j[1]) in nodes_list:
        dct = {}
        dct['id'] = "_".join(str(j[1]).split(" "))
        dct['shape'] = 'image'
        dct['image'] = 'https://www.flaticon.com/svg/vstatic/svg/3273/3273327.svg?token=exp=1619445437~hmac=9089eec5436237e9ef18383155ee6574'
        dct['value'] = str(len(df2[df2['callback_companyName']==j[1]]))
        dct['title'] = str(j[1])
        dct['label'] = str(j[1])
        # dct['color']='#FF4500'
        nodes_list.append(str(j[1]))
        nodes.append(dct)
    r_l=[x for x in routes_list if j[2] in x]
    for r in r_l:
        if not (str(j[1]),str(r)) in edges_list:
            dct={}
            dct['from'] = "_".join(str(j[1]).split(" "))
            dct['to'] = "_".join(str(r).split(" "))
            dct['value']=str(j[3])
            dct['title']=str(j[4])
            edges_list.append((str(j[1]),str(r)))
            edges.append(dct)      
# for x in lanes:
#     origin_list.append(x.split('->')[0])

df_6=df2[df2['origin_data'].isin(origin_list)]
df_7=df_6.groupby(['callback_companyName','origin_data']).agg({"asset_assetId":"count",
	                                                             "asset_equipment_equipmentType":"max"}).sort_values(by='asset_assetId',ascending=False)[['asset_assetId','asset_equipment_equipmentType']].reset_index()
# df_7=df_7[0:10]
for k in tqdm(origin_list,total=len(origin_list)):
    df_tmp_=df_7[df_7['origin_data']==k]
    df_tmp=df_tmp_[0:20]
    for j in df_tmp.itertuples():
	    if not str(j[1]) in nodes_list:
	        dct = {}
	        dct['id'] = "_".join(str(j[1]).split(" "))
	        dct['shape'] = 'image'
	        dct['image'] = 'https://www.flaticon.com/svg/vstatic/svg/4231/4231188.svg?token=exp=1619451061~hmac=f0cc5bd0ccfdfbbfd28839e979a2c6ce'
	        dct['value'] = str(len(df2[df2['callback_companyName']==j[1]]))
	        dct['title'] = str(j[1])
	        # dct['color']='#FF4500'
	        nodes_list.append(str(j[1]))
	        nodes.append(dct)
	    r_l=[x for x in routes_list if j[2] in x]
	    for r in r_l:
	        if not (str(j[1]),str(r)) in edges_list:
	            dct={}
	            dct['from'] = "_".join(str(j[1]).split(" "))
	            dct['to'] = "_".join(str(r).split(" "))
	            dct['value']=str(j[3])
	            dct['title']=str(j[4])
	            edges_list.append((str(j[1]),str(r)))
	            edges.append(dct)  

physics='false'

































