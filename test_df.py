# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# """
# Created on Sat Apr 24 21:07:19 2021

# @author: yousuf
# """
# import pandas as pd
# df1 = pd.read_csv("/home/yousuf/Downloads/scalecapacity/text_proj/omaha_loads_2021_01.csv",
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })
# df2 = pd.read_csv("/home/yousuf/Downloads/scalecapacity/text_proj/omaha_loads_2021_01.csv",
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })
# df=pd.concat([df1,df2])
# df['origin_data'] = df[['asset_shipment_origin_namedCoordinates_stateProvince','asset_shipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['destination_data'] = df[['asset_shipment_destination_namedCoordinates_stateProvince','asset_shipment_destination_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)

# df['origin_data'] = df['origin_data'].str.upper()

# df['destination_data'] = df['destination_data'].str.upper()

# df['travel_data'] = df['travel_data'].str.upper()

# df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)

# df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))

# df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)

# df=df.fillna(0)
# df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
# df['created_week']=df['created_week'].astype(int)
# df=df[df['created_week']<=8]

# df=df[df['origin_data']=='NE OMAHA']

# # import matplotlib
# # import numpy as np
# # COLOURS=list(matplotlib.colors.cnames.values()) 
# # np.random.shuffle(COLOURS)


# # df_map=df.groupby(['created_week','destination_data']).agg({"asset_assetId":"count"}).reset_index()
# # df_map=df_map.sort_values(by=['asset_assetId'],ascending=False)
# # datasets=[]
# # labels=df_map['destination_data'][0:20]
# # for k in enumerate(labels):
# #     i=k[1]
# #     df_tmp=df_map[df_map['destination_data']==i]
# #     deet=[]
# #     for j in range(1,9):
# #         try:            
# #             deet.append(df_tmp[df_tmp['created_week']==j]['asset_assetId'].values[0])
# #         except IndexError:
# #             deet.append(0)
# #     datasets.append({
# #       "label": i,
# #       "data": deet,
# #       "backgroundColor": COLOURS[k[0]],
# #     })
# # final_data={"labels":labels,
# #             "datasets":datasets}


# df_radar_ld=df.groupby(['asset_shipment_equipmentType']).agg({"asset_assetId":"count"}).reset_index()
# label_l=df_radar_ld['asset_shipment_equipmentType'].values.tolist()

# df1 = pd.read_csv("/home/yousuf/Downloads/scalecapacity/text_proj/omaha_trucks_2021_01.csv",
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })
# df2 = pd.read_csv("/home/yousuf/Downloads/scalecapacity/text_proj/omaha_trucks_2021_01.csv",
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })
# df=pd.concat([df1,df2])
# df['origin_data'] = df[['asset_equipment_origin_namedCoordinates_stateProvince','asset_equipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['destination_data'] = df[['asset_equipment_destination_place_namedCoordinates_stateProvince','asset_equipment_destination_place_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)

# df['origin_data'] = df['origin_data'].str.upper()

# df['destination_data'] = df['destination_data'].str.upper()

# df['travel_data'] = df['travel_data'].str.upper()

# df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)

# df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))

# df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)

# df=df.fillna(0)
# df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
# df['created_week']=df['created_week'].astype(int)
# df=df[df['created_week']<=8]

# df=df[df['origin_data']=='NE OMAHA']
# df_radar_tr=df.groupby(['asset_equipment_equipmentType']).agg({"asset_assetId":"count"}).reset_index()
# label_t=df_radar_tr['asset_equipment_equipmentType'].values.tolist()

# labelk=list(set(label_l+label_t))
# data_l=[]
# data_t=[]

# for i in labelk:
#     try:
#         data_t.append(df_radar_tr[df_radar_tr['asset_equipment_equipmentType']==i]['asset_assetId'].values[0])
#     except:
#         data_t.append(0)
#     try:
#         data_l.append(df_radar_ld[df_radar_ld['asset_shipment_equipmentType']==i]['asset_assetId'].values[0])
#     except:
#         data_l.append(0)
# final_data3={
#   "labels": labelk,
#   "datasets": [
#     {
#       "label": 'Trucks',
#       "data": data_t,
#       "borderColor": "rgb(51, 153, 255, 1)",
#       "backgroundColor": "rgb(51, 153, 255, 0.2)",
#     },
#     {
#       "label": 'Loads',
#       "data": data_l,
#       "borderColor":'rgba(255, 99, 132, 1)',
#       "backgroundColor": "rgba(255, 99, 132, 0.2)",
#     }
#   ]
# }







from tqdm import tqdm
import pandas as pd
FL=["FL_loads_2021_01.csv","FL_loads_2021_02_1.csv"]
CA=["CA_loads_2021_01.csv","CA_loads_2021_02_1.csv"]
IL=["IL_loads_2021_01.csv","IL_loads_2021_02_1.csv"]
NE=["NE_loads_2021_01.csv","NE_loads_2021_02.csv"]
omaha=["omaha_loads_2021_01.csv","omaha_loads_2021_02_1.csv","omaha_loads_2021_02_2.csv"]
dirz="/home/yousuf/Downloads/scalecapacity/text_proj/loads/"
loads_dtype={
			                     'created_week': int,
                                  'duration': float,
                                  'asset_shipment_origin_namedCoordinates_latitude': float,
                                  'asset_shipment_origin_namedCoordinates_longitude': float,
                                  'asset_shipment_destination_namedCoordinates_latitude': float,
                                  'asset_shipment_destination_namedCoordinates_longitude': float,
                                  'asset_shipment_origin_namedCoordinates_stateProvince': str,
                                  'asset_shipment_origin_namedCoordinates_city': str,
                                  'asset_shipment_destination_namedCoordinates_stateProvince': str,
                                  'asset_shipment_destination_namedCoordinates_city': str,
                                  'asset_shipment_equipmentType': str,
                                  'asset_assetId': str,
                                  'callback_userId': str,
                                  'callback_companyName': str,
			                     'destination_data': str,
                                  'travel_data': str,
			                     'origin_data': str,
                                  'created_at': str
			                 }
dct={}
dct['FL']=FL
dct['CA']=CA
dct['IL']=IL
dct['NE']=NE
dct['omaha']=omaha
all_frames=[]
for j in tqdm(dct.items(),total=len(dct)):
    frames=[]
    for i in j[1]:
        df1 = pd.read_csv(dirz+i,
    				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
    			                 dtype={
    			                     'callback_userId': str,
    			                     'creditScore_score': float,
    			                     'asset_dimensions_weightPounds': float
    			                 })
        frames.append(df1)
    df=pd.concat(frames)
    df['origin_data'] = df[['asset_shipment_origin_namedCoordinates_stateProvince','asset_shipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
    
    df['destination_data'] = df[['asset_shipment_destination_namedCoordinates_stateProvince','asset_shipment_destination_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
    
    df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)
    
    df['origin_data'] = df['origin_data'].str.upper()
    
    df['destination_data'] = df['destination_data'].str.upper()
    
    df['travel_data'] = df['travel_data'].str.upper()
    
    df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)
    
    df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))
    
    df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)
    
    df=df.fillna(0)
    df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
    df['created_week']=df['created_week'].astype(int)
    df['created_year']=df['asset_status_created_date'].apply(lambda x : x.strftime("%Y"))
    df['created_year']=df['created_year'].astype(int)
    df=df[df['created_year']==2021]
    # df=df[df['created_week']<=10]
    clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
            'asset_shipment_equipmentType',
            'asset_shipment_origin_namedCoordinates_stateProvince',
            'asset_shipment_origin_namedCoordinates_city',
            'asset_shipment_origin_namedCoordinates_latitude',
            'asset_shipment_origin_namedCoordinates_longitude',
            'asset_shipment_destination_namedCoordinates_stateProvince',
            'asset_shipment_destination_namedCoordinates_city',
            'asset_shipment_destination_namedCoordinates_latitude',
            'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
            'destination_data', 'travel_data', 'duration', 'created_at',
            'created_week']
    df=df[clss]
    all_frames.append(df)

df=pd.concat(all_frames)
df.drop_duplicates(subset=['asset_assetId', 'callback_userId'], keep='last')
df.to_csv(dirz+"MAP_loads.csv",index=0)

post_sql=False

if post_sql:
    from tqdm import tqdm
    import pandas as pd
    import os
    loads_dtype={
             'created_week': int,
              'duration': float,
              'asset_shipment_origin_namedCoordinates_latitude': float,
              'asset_shipment_origin_namedCoordinates_longitude': float,
              'asset_shipment_destination_namedCoordinates_latitude': float,
              'asset_shipment_destination_namedCoordinates_longitude': float,
              'asset_shipment_origin_namedCoordinates_stateProvince': str,
              'asset_shipment_origin_namedCoordinates_city': str,
              'asset_shipment_destination_namedCoordinates_stateProvince': str,
              'asset_shipment_destination_namedCoordinates_city': str,
              'asset_assetId': str,
              'callback_userId': str,
              'callback_companyName': str,
             'destination_data': str,
              'travel_data': str,
             'origin_data': str,
              'created_at': str
         }
    trucks_dtype={
           'created_week': int,
          'duration': float,
          'asset_equipment_origin_namedCoordinates_latitude': float,
          'asset_equipment_origin_namedCoordinates_longitude': float,
          'asset_equipment_destination_place_namedCoordinates_latitude': float,
          'asset_equipment_destination_place_namedCoordinates_longitude': float,
          'asset_equipment_origin_namedCoordinates_stateProvince': str,
          'asset_equipment_origin_namedCoordinates_city': str,
          'asset_equipment_destination_place_namedCoordinates_stateProvince': str,
          'asset_equipment_destination_place_namedCoordinates_city': str,
          'asset_assetId': str,
          'callback_userId': str,
          'callback_companyName': str,
             'destination_data': str,
          'travel_data': str,
             'origin_data': str,
          'created_at': str
         }
    dirz=f"{os.getcwd()}/loads/"
    df_load = pd.read_csv(dirz+"MAP_loads.csv",
    		parse_dates=['asset_status_created_date'],
    	                 dtype=loads_dtype)
    dirz=f"{os.getcwd()}/trucks/"
    df_truck = pd.read_csv(dirz+"MAP_trucks.csv",
    				parse_dates=['asset_status_created_date'],
    			                 dtype=trucks_dtype)
    
    from sqlite3 import Error
    import sqlite3 as lite
    import os
    dbname=f'{os.getcwd()}/db.sqlite3'
    conn = lite.connect(dbname)
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS loads (
                  id INTEGER PRIMARY KEY,
                  asset_assetId TEXT NOT NULL UNIQUE,
                  callback_userId TEXT NOT NULL,
                  callback_companyName TEXT NOT NULL,
                  asset_status_created_date TEXT NOT NULL,
                  asset_shipment_equipmentType TEXT NOT NULL,
                  asset_shipment_origin_namedCoordinates_stateProvince TEXT NOT NULL,
                  asset_shipment_origin_namedCoordinates_city TEXT NOT NULL,
                  asset_shipment_origin_namedCoordinates_latitude TEXT NOT NULL,
                  asset_shipment_origin_namedCoordinates_longitude TEXT NOT NULL,
                  asset_shipment_destination_namedCoordinates_stateProvince TEXT NOT NULL,
                  asset_shipment_destination_namedCoordinates_city TEXT NOT NULL,
                  asset_shipment_destination_namedCoordinates_latitude TEXT NOT NULL,
                  asset_shipment_destination_namedCoordinates_longitude TEXT NOT NULL,
                  origin_data TEXT NOT NULL,
                  destination_data TEXT NOT NULL,
                  travel_data TEXT NOT NULL,
                  duration TEXT NOT NULL,
                  created_at TEXT NOT NULL,
                  created_week TEXT NOT NULL
                  );""")
    
    
    df=df_load.copy()
    for i in tqdm(df.itertuples(),total=len(df)):
        postgresql=f"""INSERT INTO loads (asset_assetId, callback_userId, callback_companyName, asset_status_created_date, asset_shipment_equipmentType,
        asset_shipment_origin_namedCoordinates_stateProvince, asset_shipment_origin_namedCoordinates_city,
        asset_shipment_origin_namedCoordinates_latitude, asset_shipment_origin_namedCoordinates_longitude,
        asset_shipment_destination_namedCoordinates_stateProvince, asset_shipment_destination_namedCoordinates_city,
        asset_shipment_destination_namedCoordinates_latitude, asset_shipment_destination_namedCoordinates_longitude,
        origin_data, destination_data,travel_data,duration,created_at,created_week) values (
        "{i[1]}", "{i[2]}", "{i[3]}", "{str(i[4])}", 
        "{i[5]}", "{i[6]}", "{i[7]}", "{str(i[8])}", 
        "{str(i[9])}", "{str(i[10])}", "{str(i[11])}", "{str(i[12])}", 
        "{str(i[13])}", "{i[14]}", "{i[15]}", "{i[16]}", 
        "{str(i[17])}", "{str(i[18])}", "{i[19]}") 
        ON CONFLICT (asset_assetId) DO UPDATE SET callback_userId="{i[2]}", asset_status_created_date="{str(i[4])}", 
        asset_shipment_origin_namedCoordinates_latitude="{str(i[8])}", asset_shipment_origin_namedCoordinates_longitude="{str(i[9])}", 
        asset_shipment_destination_namedCoordinates_latitude="{str(i[12])}", asset_shipment_destination_namedCoordinates_longitude="{str(i[13])}", 
        created_at="{str(i[18])}"; """
        cursor.execute(postgresql)
        break
    conn.commit()
    
    cursor.execute("""CREATE TABLE IF NOT EXISTS trucks (
                  id INTEGER PRIMARY KEY,
                  asset_assetId TEXT NOT NULL UNIQUE,
                  callback_userId TEXT NOT NULL,
                  callback_companyName TEXT NOT NULL,
                  asset_status_created_date TEXT NOT NULL,
                  asset_equipment_equipmentType TEXT NOT NULL,
                  asset_equipment_origin_namedCoordinates_stateProvince TEXT NOT NULL,
                  asset_equipment_origin_namedCoordinates_city TEXT NOT NULL,
                  asset_equipment_origin_namedCoordinates_latitude TEXT NOT NULL,
                  asset_equipment_origin_namedCoordinates_longitude TEXT NOT NULL,
                  asset_equipment_destination_place_namedCoordinates_stateProvince TEXT NOT NULL,
                  asset_equipment_destination_place_namedCoordinates_city TEXT NOT NULL,
                  asset_equipment_destination_place_namedCoordinates_latitude TEXT NOT NULL,
                  asset_equipment_destination_place_namedCoordinates_longitude TEXT NOT NULL,
                  origin_data TEXT NOT NULL,
                  destination_data TEXT NOT NULL,
                  travel_data TEXT NOT NULL,
                  duration TEXT NOT NULL,
                  created_at TEXT NOT NULL,
                  created_week TEXT NOT NULL
                  );""")
    conn.commit()
    
    df=df_truck.copy()
    for i in tqdm(df.itertuples(),total=len(df)):
        postgresql=f"""INSERT INTO trucks (asset_assetId, callback_userId, callback_companyName, asset_status_created_date, asset_equipment_equipmentType,
        asset_equipment_origin_namedCoordinates_stateProvince, asset_equipment_origin_namedCoordinates_city,
        asset_equipment_origin_namedCoordinates_latitude, asset_equipment_origin_namedCoordinates_longitude,
        asset_equipment_destination_place_namedCoordinates_stateProvince, asset_equipment_destination_place_namedCoordinates_city,
        asset_equipment_destination_place_namedCoordinates_latitude, asset_equipment_destination_place_namedCoordinates_longitude,
        origin_data, destination_data,travel_data,duration,created_at,created_week) values (
        "{i[1]}", "{i[2]}", "{i[3]}", "{str(i[4])}", 
        "{i[5]}", "{i[6]}", "{i[7]}", "{str(i[8])}", 
        "{str(i[9])}", "{str(i[10])}", "{str(i[11])}", "{str(i[12])}", 
        "{str(i[13])}", "{i[14]}", "{i[15]}", "{i[16]}", 
        "{str(i[17])}", "{str(i[18])}", "{i[19]}") 
        ON CONFLICT (asset_assetId) DO UPDATE SET callback_userId="{i[2]}", asset_status_created_date="{str(i[4])}", 
        asset_equipment_origin_namedCoordinates_latitude="{str(i[8])}", asset_equipment_origin_namedCoordinates_longitude="{str(i[9])}", 
        asset_equipment_destination_place_namedCoordinates_latitude="{str(i[12])}", asset_equipment_destination_place_namedCoordinates_longitude="{str(i[13])}", 
        created_at="{str(i[18])}"; """
        cursor.execute(postgresql)
        break
    conn.commit()

conn.close()






# # df = pd.read_csv(dirz+FL[0],
# # 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# # 			                 dtype={
# # 			                     'callback_userId': str,
# # 			                     'creditScore_score': float,
# # 			                     'asset_dimensions_weightPounds': float
# # 			                 })

# # df = pd.read_csv(dirz+"MAP_FL_loads.csv",
# # 				parse_dates=['asset_status_created_date'],
# # 			                 dtype=loads_dtype)

# # df = pd.read_csv(dirz+CA[0],
# # 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# # 			                 dtype={
# # 			                     'callback_userId': str,
# # 			                     'creditScore_score': float,
# # 			                     'asset_dimensions_weightPounds': float
# # 			                 })

# # df = pd.read_csv(dirz+"MAP_CA_loads.csv",
# # 				parse_dates=['asset_status_created_date'],
# # 			                 dtype=loads_dtype)

# df1 = pd.read_csv(dirz+NE[0],
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })
# df2 = pd.read_csv(dirz+NE[1],
# 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# 			                 dtype={
# 			                     'callback_userId': str,
# 			                     'creditScore_score': float,
# 			                     'asset_dimensions_weightPounds': float
# 			                 })

# df=pd.concat([df1,df2])
# df['origin_data'] = df[['asset_shipment_origin_namedCoordinates_stateProvince','asset_shipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['destination_data'] = df[['asset_shipment_destination_namedCoordinates_stateProvince','asset_shipment_destination_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)

# df['origin_data'] = df['origin_data'].str.upper()

# df['destination_data'] = df['destination_data'].str.upper()

# df['travel_data'] = df['travel_data'].str.upper()

# df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)

# df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))

# df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)

# df=df.fillna(0)
# df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
# df['created_week']=df['created_week'].astype(int)
# df['created_year']=df['asset_status_created_date'].apply(lambda x : x.strftime("%Y"))
# df['created_year']=df['created_year'].astype(int)
# df=df[df['created_year']==2021]
# df=df[df['created_week']<=8]
# clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
#         'asset_shipment_equipmentType',
#         'asset_shipment_origin_namedCoordinates_stateProvince',
#         'asset_shipment_origin_namedCoordinates_city',
#         'asset_shipment_origin_namedCoordinates_latitude',
#         'asset_shipment_origin_namedCoordinates_longitude',
#         'asset_shipment_destination_namedCoordinates_stateProvince',
#         'asset_shipment_destination_namedCoordinates_city',
#         'asset_shipment_destination_namedCoordinates_latitude',
#         'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
#         'destination_data', 'travel_data', 'duration', 'created_at',
#         'created_week']
# df=df[clss]
# df.to_csv(dirz+"MAP_NE_loads.csv",index=0)


# # df = pd.read_csv(dirz+"MAP_IL_loads.csv",
# # 				parse_dates=['asset_status_created_date'],
# # 			                 dtype=loads_dtype)




# # df = pd.read_csv(dirz+NE[0],
# # 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# # 			                 dtype={
# # 			                     'callback_userId': str,
# # 			                     'creditScore_score': float,
# # 			                     'asset_dimensions_weightPounds': float
# # 			                 })


# # df = pd.read_csv(dirz+"MAP_NE_loads.csv",
# # 				parse_dates=['asset_status_created_date'],
# # 			                 dtype=loads_dtype)



# # df1 = pd.read_csv(dirz+omaha[0],
# # 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# # 			                 dtype={
# # 			                     'callback_userId': str,
# # 			                     'creditScore_score': float,
# # 			                     'asset_dimensions_weightPounds': float
# # 			                 })

# # df2 = pd.read_csv(dirz+omaha[1],
# # 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# # 			                 dtype={
# # 			                     'callback_userId': str,
# # 			                     'creditScore_score': float,
# # 			                     'asset_dimensions_weightPounds': float
# # 			                 })

# # df3 = pd.read_csv(dirz+omaha[2],
# # 				parse_dates=['asset_status_endDate','asset_status_startDate','asset_status_created_date'],
# # 			                 dtype={
# # 			                     'callback_userId': str,
# # 			                     'creditScore_score': float,
# # 			                     'asset_dimensions_weightPounds': float
# # 			                 })

# # df=pd.concat([df1,df2,df3])


# df['origin_data'] = df[['asset_shipment_origin_namedCoordinates_stateProvince','asset_shipment_origin_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['destination_data'] = df[['asset_shipment_destination_namedCoordinates_stateProvince','asset_shipment_destination_namedCoordinates_city']].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)

# df['travel_data'] = df[['origin_data','destination_data']].apply(lambda row: '->'.join(row.values.astype(str)), axis=1)

# df['origin_data'] = df['origin_data'].str.upper()

# df['destination_data'] = df['destination_data'].str.upper()

# df['travel_data'] = df['travel_data'].str.upper()

# df['duration']=(df['asset_status_endDate'] - df['asset_status_startDate']) / pd.Timedelta(hours=1)

# df['created_at'] = df['asset_status_created_date'].map(lambda x: x.strftime('%Y-%m-%d'))

# df.sort_values(by=['asset_status_created_date'], inplace=True, ascending=False)

# df=df.fillna(0)
# df['created_week']=df['asset_status_created_date'].apply(lambda x : x.strftime("%V"))
# df['created_week']=df['created_week'].astype(int)
# df=df[df['created_week']<=8]
# clss=[ 'asset_assetId','callback_userId','callback_companyName','asset_status_created_date',
#         'asset_shipment_equipmentType',
#         'asset_shipment_origin_namedCoordinates_stateProvince',
#         'asset_shipment_origin_namedCoordinates_city',
#         'asset_shipment_origin_namedCoordinates_latitude',
#         'asset_shipment_origin_namedCoordinates_longitude',
#         'asset_shipment_destination_namedCoordinates_stateProvince',
#         'asset_shipment_destination_namedCoordinates_city',
#         'asset_shipment_destination_namedCoordinates_latitude',
#         'asset_shipment_destination_namedCoordinates_longitude', 'origin_data',
#         'destination_data', 'travel_data', 'duration', 'created_at',
#         'created_week']
# df=df[clss]
# df.to_csv(dirz+"MAP_NE_loads.csv",index=0)



# df1 = pd.read_csv(dirz+"MAP_NE_loads.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=loads_dtype)

# df2 = pd.read_csv(dirz+"MAP_FL_loads.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=loads_dtype)

# df3 = pd.read_csv(dirz+"MAP_IL_loads.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=loads_dtype)

# df4 = pd.read_csv(dirz+"MAP_CA_loads.csv",
# 				parse_dates=['asset_status_created_date'],
# 			                 dtype=loads_dtype)

# df=pd.concat([df1,df2,df3,df4])

# df.to_csv(dirz+"MAP_loads.csv",index=0)




















































